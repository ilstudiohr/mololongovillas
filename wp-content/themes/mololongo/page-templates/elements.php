<?php
/**
 * Template Name: Elements
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>

<div class="wrapper" id="full-width-page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content">

		<div class="row">

			<div class="col-md-12 content-area" id="primary">

				<main class="site-main" id="main" role="main">

					<?php
					while ( have_posts() ) {
						the_post();
						get_template_part( 'loop-templates/content', 'page' );
					}
					?>
					
					<div class="app-box">
					    <div class="app-image">
					        <img src="http://dev.mololongo.com/wp-content/uploads/2021/07/apartman-tanja-1-1024x683.jpg">
					        <span class="app-stars"><i class="fa fa-star"></i></span>
					    </div>
					    <div class="app-content">
					        <div class="app-title">
					            <h4>Apartman West Wing</h4>
					            <span class="app-price">90 eur/noć</span>
					        </div>
					        <div class="app-icons">
					            <ul class="small-icons">
					                <li><i class="fa fa-plane"></i> Crikvenica</li>
					                <li><i class="fa fa-plane"></i> Crikvenica</li>
					                <li><i class="fa fa-plane"></i> Crikvenica</li>
					                <li><i class="fa fa-plane"></i> Crikvenica</li>
					                <li><i class="fa fa-plane"></i> Crikvenica</li>
					            </ul>
					        </div>
					        <div class="app-short">
					            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
					        </div>
					        <div class="app-button">
					            <a href="#" class="button-primary">Rezerviraj</a>
					        </div>
					    </div>
					    
					</div>

				</main><!-- #main -->

			</div><!-- #primary -->

		</div><!-- .row end -->

	</div><!-- #content -->

</div><!-- #full-width-page-wrapper -->

<?php
get_footer();
