<?php
/**
 * Template Name: Home Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );
search_submit();
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="home-hero">
    <video width="100%" autoplay loop muted>
        <source src="<?php echo get_template_directory_uri(); ?>/images/mololongovillas.mp4" type="video/mp4">
        Your browser does not support HTML video.
    </video>
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                
                <div class="hero-title">
                    <div class="filter-box filter-box--or--homepage">
                        <button class="mobile--modal"><span><img src="<?php bloginfo('template_directory');?>/images/search_magnifier.svg" alt="booking manager" /></span><?php _e('Lokacija', 'molonew'); ?></button>
                        <div class="filter-wrapper mobile--form">
                            <button style="display: none;" class="mobile--modal-close"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="aboutUs"></button>
                            <form method="GET" class="mphb_sc_search-form" action="<?php echo get_site_url(); ?>/rezultati-pretrage/">
                                <!-- <p class="mphb_sc_search-lokacija mphb_sc_search-grad">
                                     <select id="mphb_lokacija-mphb-search-form" name="mphb_ra_lokacija">
                                         <option value=""><?php _e('Lokacija', 'molonew'); ?></option>
                                         <option value="istra"><?php _e('Istra', 'molonew'); ?></option>
                                         <option value="kvarner"><?php _e('Kvarner', 'molonew'); ?></option>
                                         <option value="dalmacija"><?php _e('Dalmacija', 'molonew'); ?></option>
                                     </select>
                                 </p>-->
                            <p class="mphb_sc_search-location ui-widget border-tr mphb_sc_search-location-homepage--or">
                                <input id="mphb_grad-mphb-search-form" class="left-radius" name="q" autocomplete="off" placeholder="<?php _e('Lokacija', 'molonew'); ?>">
                            </p>
                           <!-- <p class="mphb_sc_search-location">
                               <input id="mphb_grad-mphb-search-form" name="q" autocomplete="off">
                           </p> -->

                            <div class="t-datepicker">
                              <p class="mphb_sc_search-check-in-date t-check-in"></p>
                              <p class="mphb_sc_search-check-out-date t-check-out"></p>
                            </div>         
                   
                            <input type="hidden" id="mphb_adults-mphb-search-form" name="mphb_adults" value="1" />
                            <input type="hidden" id="mphb_children-mphb-search-form" name="mphb_children" value="0" />
                            
                            <div class="modal-toggle-range--wrapp modal-toggle-range--wrapp--homepage modal-guests modal-toggle-range-no-icon">
                               <button class="advanced-filters--or modal-toggle-range " type="button" id="dropdownMenuButton">
                                 <?php _e('Gosti', 'molonew'); ?> <span id="guestsOr"></span> <input type="text" id="Sum" value="" disabled/><i class="fa fa-angle-down guest-icon"></i>
                               </button>
                               <script>
                                function reSum()
                                {
                                    var num1 = parseInt(document.getElementById("adults").value);
                                    var num2 = parseInt(document.getElementById("children").value);
                                    document.getElementById("Sum").value = num1 + num2;
                        
                                }
                              </script>
                               
                                <div class="dropdown-menu dropdown-menu--or" >
                                    <ul class="block detalji_smjestaja accommodation-details--or accommodation-details--or--homepage">
                                        <li class="sf-field-post-meta-basic_info_beds">
                                           <p><?php _e('Odrasli', 'molonew'); ?></p>
                                           <label>
                                            <div class="quantity">
                                              <input class="quantity__capacity" id="adults" type="number" onchange="reSum();" name="adults" min="1" step="1" max="20" value="1">
                                          </div>
                                          </label>
                                        </li>
                                        <li class="sf-field-post-meta-basic_info_beds">
                                           <p class="modal-guests__info">
                                            <span><?php _e('Djeca', 'molonew'); ?></span>
                                            <span><?php _e('ispod 18 godina', 'molonew'); ?></span>
                                        </p>
                                           <label>
                                            <div class="quantity">
                                              <input class="quantity__capacity" id="children" type="number" name="children" onchange="reSum();" min="0" step="1" max="20" value="0">
                                            </div>
                                          </label>
                                        </li>
                                        <li>
                                            <input type="hidden" id="capacity" type="number" min="1" max="40" value="" name="mphb_attributes[kapacitet]">
                                        </li>
                                        <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">
                                          <div class="checkbox--or">
                                            <input  class="sf-input-checkbox" type="checkbox" value="pet_friendly" name="_sfm_search_amenities[]" id="sf-input-4922c9d1395f7191f2e7f935350a656d">
                                            <label for="sf-input-4922c9d1395f7191f2e7f935350a656d"><?php _e('Pet friendly', 'molonew'); ?></label>
                                          </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>                           
                            <input id="mphb_check_in_date-mphb-search-form-hidden" value="" type="hidden" name="mphb_check_in_date" />
                            <input id="mphb_check_out_date-mphb-search-form-hidden" value="" type="hidden" name="mphb_check_out_date" />
                            <p class="mphb_sc_search-submit-button-wrapper mphb_sc_search-submit-button-wrapper--homepage">
                            <input type="submit" class="button" value="<?php _e('Pretraži', 'molonew'); ?>"/>
                            </p>
 			    <?php get_template_part( 'search-res-templates/adv', 'search-homepage' ); ?>
                        </form>
                        </div>
                    </div>
                    <h4><?php _e('Doživite', 'molonew'); ?></h4>
                    <h1 class="cb-slideshow ml4">
                        <span class="letters letters-1 subtitle"><?php _e('Dalmaciju', 'molonew'); ?></span>
                        <span class="letters letters-2 subtitle"><?php _e('Istru', 'molonew'); ?></span>
                        <span class="letters letters-3 subtitle"><?php _e('Kvarner', 'molonew'); ?></span>
                    </h1>
                    <!--<p class="desc"><?php _e('Odaberite smještaj po svojoj mjeri i osjećajte se #kaodoma.', 'molonew'); ?></p>-->
                </div>
                
                
            </div>
        </div>
    </div>
</section>
<section class="app-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                    <span class="over-title"><?php _e('VILE IZ MOLO LONGO PONUDE', 'molonew'); ?></span>
                    <h2 class="h1"><?php _e('Za vas izdvajamo', 'molonew'); ?></h2>
                <?php endwhile; endif; ?>
                    
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-2">
                <div class="swiper-wrapper">
                <?php $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type','tax_query' => array(array('taxonomy' => 'mphb_ra_karakteristika', 'field' => 'slug', 'terms' => array('posebna-ponuda')))); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('gallery'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-pagination"></div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				       <!-- <p class="product-price-or"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></p>-->
            				    </div>
            				    
            				    <div class="app-content">
            				        <a class="link-abs" href="<?php the_permalink() ?>"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				            <!--<span class="app-price"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></span>-->
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('gosti') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('gosti'); if( get_sub_field('gosti_max') ): ?> + <?php the_sub_field('gosti_max'); endif; ?></li>
            				                <?php endif; ?>  
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(22); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				        
            				    </div>
            				    
            				    
            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>

<section class="small-space">
    <div class="container">
        <div class="row">
            <div class="col-md-6 mobile-two">
                <div class="page-content mb-3">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <span class="over-title"><?php the_sub_field('podnaslov'); ?></span>
                        <h3 class="h1"><?php the_sub_field('naslov'); ?></h2>
                        <p><?php the_sub_field('tekst'); ?></p>
                        <a href="/accommodation/" class="button-primary mt-3"><?php _e('Pretraži vile', 'molonew'); ?></a>
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>
            <div class="col-md-6 mobile-one">
                <div class="page-content img-shape">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <?php 
                        $image = get_sub_field('slika');
                        if( !empty( $image ) ): ?>
                            <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                        <?php endif; ?>
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="gray-background">
    <div class="container">
        <div class="row small_gutter">
            <div class="col-md-5 mobile-two">
                <?php if( have_rows('sekcija_1') ): while ( have_rows('sekcija_1') ) : the_row(); ?>
                    <div class="image-title">
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                        </a>
                        
                        <div class="image-content">
                            <a href="<?php the_sub_field('link');?>">
                                <h5 class="h2"><?php the_sub_field('naslov');?></h5>
                            </a>
                            
                        </div>
                    </div>
                <?php endwhile; endif; ?>
                
                <?php if( have_rows('sekcija_2') ): while ( have_rows('sekcija_2') ) : the_row(); ?>
                    <div class="image-title half-height">
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                        </a>
                        
                        <div class="image-content">
                            <a href="<?php the_sub_field('link');?>">
                               <h5 class="h2"><?php the_sub_field('naslov');?></h5>
                            </a>
                            
                        </div>
                    </div>
                <?php endwhile; endif; ?>
                
                
            </div>
            <div class="col-md-3 mobile-two">
                <?php if( have_rows('sekcija_3') ): while ( have_rows('sekcija_3') ) : the_row(); ?>
                    <div class="image-title">
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                        </a>
                        
                        <div class="image-content">
                            <a href="<?php the_sub_field('link');?>">
                                <h5 class="h2"><?php the_sub_field('naslov');?></h5>
                            </a>
                            
                        </div>
                    </div>
                <?php endwhile; endif; ?>
                <?php if( have_rows('sekcija_4') ): while ( have_rows('sekcija_4') ) : the_row(); ?>
                    <div class="image-title half-height">
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                        </a>
                        
                        <div class="image-content">
                            <a href="<?php the_sub_field('link');?>">
                                <h5 class="h2"><?php the_sub_field('naslov');?></h5>
                            </a>
                            
                        </div>
                    </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-4 mobile-one">
                <div class="content-center pl-md-4">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <span class="over-title"><?php _e('PONUDA KROJENA PO VAŠOJ MJERI', 'molonew'); ?></span>
                        <h3 class="h1"><?php _e('Sve želje ispunjene', 'molonew'); ?></h3>
                        <p class="intro-text"><?php _e('Imate ljubimca? Želite vilu s pogledom na more? Ili ste pak zabrinuti za sigurnost svojeg dvogodišnjaka? Na sve smo mislili.', 'molonew'); ?></p>
                    <?php endwhile; endif; ?>
                </div>
                
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12 intro-text">
                <h4 class="text-center h1"><?php _e('Regije', 'molonew'); ?></h4>
                <p class="text-center"><?php _e('Vrhunske lokacije. Pregršt događanja. Nezaboravno iskustvo.', 'molonew'); ?></p>
                <p class="text-center"><?php _e('Izbor je na vama.', 'molonew'); ?></p>
            </div>
        </div>
        <div class="row small_gutter mt-5">
            <div class="col-md-4">
                <div class="image-title half-height">
                    <?php if( have_rows('regija_1') ): while ( have_rows('regija_1') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                            <div class="image-content">
                            <h5 class="h2 text-white"><?php _e('Dalmacija', 'molonew'); ?></h5>
                            <p><?php _e('Doživite čari Dalmacije i priuštite si nezaboravan odmor.', 'molonew'); ?></p>
                        </div>
                        </a>
                        
                    <?php endwhile; endif; ?>
                </div>
                <div class="image-title half-height">
                    <?php if( have_rows('regija_2') ): while ( have_rows('regija_2') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                            <div class="image-content">
                            <h5 class="h2 text-white"><?php _e('Dubrovačka rivijera', 'molonew'); ?></h5>
                            <p><?php _e('Svjetski poznata destinacija koja će vas ostaviti bez daha.', 'molonew'); ?></p>
                        </div>
                        </a>
                           
                    <?php endwhile; endif; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image-title full-height">
                     <?php if( have_rows('regija_3') ): while ( have_rows('regija_3') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                            <div class="image-content">
                            <h5 class="h2 text-white"><?php _e('Istra', 'molonew'); ?></h5>
                            <p><?php _e('Jedinstven poluotok na vašem dlanu.', 'molonew'); ?></p>
                        </div>
                        </a>
                           
                    <?php endwhile; endif; ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="image-title half-height">
                    <?php if( have_rows('regija_4') ): while ( have_rows('regija_4') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                            <div class="image-content">
                            <h5 class="h2 text-white"><?php _e('Kvarner', 'molonew'); ?></h5>
                            <p><?php _e('Ljepote mora i kopna na dohvat ruke.', 'molonew'); ?></p>
                        </div>
                        </a>
                           
                    <?php endwhile; endif; ?>
                </div>
                <div class="image-title half-height">
                    <?php if( have_rows('regija_5') ): while ( have_rows('regija_5') ) : the_row(); ?>
                        <a href="<?php the_sub_field('link');?>">
                            <img src="<?php the_sub_field('slika');?>">
                            <div class="image-content">
                            <h5 class="h2 text-white"><?php _e('Kontinentalna Hrvatska', 'molonew'); ?></h5>
                            <p><?php _e('Ništa manje impresivna od obale, kontinentalna Hrvatska očarat će vas zelenilom.', 'molonew'); ?></p>
                        </div>
                        </a>
                           
                    <?php endwhile; endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="small-space">
    <div class="bg-overlay"></div>
    <div class="container">
        <div class="row align-items-md-center">
            <div class="col-md-6">
                <div class="page-content">
                    <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                        <span class="over-title"><?php _e('POSVEĆENOST VISOKIM STANDARDIMA KVALITETE', 'molonew'); ?></span>
                        <h3 class="h1"><?php _e('Vaše iskustvo naš je prioritet', 'molonew'); ?></h3>
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>
            <div class="col-md-6">
                <div class="page-content img-shape">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/villa.jpg">
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="content-box">
                    <span class="title_underline"><?php _e('Bogata ponuda', 'molonew'); ?></span>
                    <h4><?php _e('Vrhunske lokacije', 'molonew'); ?></h4>
                    <p><?php _e('U ponudi pronađite sve hrvatske regije i doživite Hrvatsku u cijelosti. Od kontinentalne Hrvatske, preko Istre i Dalmacije sve do Dubrovnika. Samo vrhunske lokacije za vaš istinski odmor.', 'molonew'); ?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="content-box">
                    <span class="title_underline"><?php _e('Strogi kriteriji', 'molonew'); ?></span>
                    <h4><?php _e('Iskustvo od 5 zvjezdica', 'molonew'); ?></h4>
                    <p><?php _e('Svaku smještajnu jedinicu koja se nalazi u Molo Longo ponudi detaljno provjeravamo kako bismo se uvjerili da je u skladu sa strogim kriterijima naše turističke ponude. ', 'molonew'); ?></p>
                    <p> <?php _e('Molo Longo. Provjerena kvaliteta!', 'molonew'); ?></p>
                </div>
            </div>
            <div class="col-md-4">
                <div class="content-box">
                    <span class="title_underline"><?php _e('Online sigurnost', 'molonew'); ?></span>
                    <h4><?php _e('Sigurnost pri plaćanju', 'molonew'); ?></h4>
                    <p><?php _e('Bezbrižno u svega nekoliko koraka potvrdite svoju rezervaciju i plaćajte on-line putem najsuvremenijeg sustava. Svaka transakcija je sigurna, a vaši podaci zaštićeni od zlouporabe.', 'molonew'); ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="app-list">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if( have_rows('intro_sekcija') ): while ( have_rows('intro_sekcija') ) : the_row(); ?>
                    <span class="over-title"><?php _e('VILE IZ MOLO LONGO PONUDE', 'molonew'); ?></span>
                    <h3 class="h1"><?php _e('Udobnost doma. Luksuz vile.', 'molonew'); ?></h3>
                <?php endwhile; endif; ?>
                    
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4">
                <div class="swiper-wrapper">
                <?php $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type'); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('gallery'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-pagination"></div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				        <!--<p class="product-price-or"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></p>-->
            				    </div>
            				    <div class="app-content">
            				        <a class="link-abs" href="<?php the_permalink() ?>"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				            <!--<span class="app-price"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></span>-->
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('gosti') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('gosti'); if( get_sub_field('gosti_max') ): ?> + <?php the_sub_field('gosti_max'); endif; ?></li>
            				                <?php endif; ?>  
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(30); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				    </div>
            				    
            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>


<!-- <section class="gray-background latest-blog">
    <div class="container">
        <div class="row">
            
            
            <?php
            $args = array(
                'posts_per_page' => 1, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post', /* your post type name */
                'post_status' => 'publish'
            );
            $query = new WP_Query($args);
                if ($query->have_posts()) :
                    while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="col-md-6">
                    <div class="img-shape w600">
                        <?php the_post_thumbnail('medium'); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="page-content">
                        <span class="over-title"><?php _e('MOLO LONGO BLOG', 'molonew'); ?></span>
                        <h3><?php the_title();?></h3>
                        <p><?php echo excerpt(100); ?></p>
                        <a href="<?php the_permalink();?>" class="button-primary"><?php _e('Pročitaj članak', 'molonew'); ?></a>
                        <a href="blog" class="ml-md-5 mt-4 button-primary"><?php _e('Svi članci', 'molonew'); ?></a>
                    </div>
                </div>
                <?php
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section> -->

<section class="small-space latest-blog pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-content intro-text" >
                    <h4 class="text-center h1"><?php _e('Novosti iz područja putovanja i turizma', 'molonew'); ?></h4>
                </div>
            </div>
            <?php
            $args = array(
                'posts_per_page' => 3, /* how many post you need to display */
                'offset' => 0,
                'orderby' => 'post_date',
                'order' => 'DESC',
                'post_type' => 'post', /* your post type name */
                'post_status' => 'publish'
            );
            $query = new WP_Query($args);
                if ($query->have_posts()) :
                    $i = 1;
                    while ($query->have_posts()) : $query->the_post();
                ?>
                <div class="<?php if ($i == 1){echo 'col-md-6';}elseif ($i == 2){echo 'col-md-4';}else{echo 'col-md-2';};?>">

                    <div class="image-title">
                        <div class="overlay"></div>
                        <?php the_post_thumbnail('medium'); ?>
                        <div class="image-content">
                            <h5><?php the_title();?></h5>
                            <p><?php echo excerpt(15); ?></p>
                            <div class="image-button">
                                <a href="<?php the_permalink();?>" class="button"><?php _e('Pročitaj', 'molonew'); ?></a>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                    $i++;
                endwhile;
            endif;
            ?>
        </div>
    </div>
</section>
<section class="sep latest-blog-sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>

<section class="parallax-cta" style="background: url('<?php echo get_stylesheet_directory_uri(); ?>/images/parallax.jpg') ;   
    background-repeat: no-repeat;
    background-size: cover;background-attachment: fixed;">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h4><?php _e('Iznajmite i vi svoju vilu', 'molonew'); ?></h4>
                <a href="<?php echo esc_url( home_url( '/' ) ); ?><?php _e('iznajmite-nekretninu', 'molonew'); ?>" class="button-primary big-button"><?php _e('Kontakt', 'molonew'); ?></a>
            </div>
            
        </div>
    </div>
</section>
<?php
get_footer();
