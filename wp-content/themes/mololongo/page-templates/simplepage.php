<?php
/**
 * Template Name: Simple Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><?php the_title();?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
					while ( have_posts() ) {
						the_post();
						the_content();
					}
					?>
            </div>
        </div>
    </div>
</section>
<?php if( get_field('iframe') ): ?>
<section class="py-0">
    	<?php the_field('iframe'); ?>
</section>
<?php endif; ?>
<?php
get_footer();
