<?php
/**
 * Template Name: Apartment Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

?>
<section class="app-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="apartment-title">
                    <h1>Apartman Tanja</h1>
                    <span class="stars">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                    </span>
                </div>
                <div class="app-meta">
                    <ul>
                        <li><i class="fa fa-bed"></i> <span>1+1</span></li>
                        <li><i class="fa fa-square"></i> <span>60m<sup>2</sup></span></li>
                        <li><i class="fa fa-users"></i> <span>4+1</span></li>
                        <li><i class="fa fa-bath"></i> <span>1</span></li>
                        <li><i class="fa fa-car"></i> <span>2</span></li>
                    </ul>
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="app_feat-img">
                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                </div>
            </div>
            <div class="col-md-4">
                <div class="app-gallery">
                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                     <div class="mt-3"><?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?></div>
                </div>
            </div>
        </div>
        <div class="row app-content mt-2">
            <div class="col-md-9">
                <div class="app-info app-border">
                    <div class="app-meta block-meta">
                        <ul>
                            <li><i class="fa fa-phone"></i> <span>+385 91 600 6646</span></li>
                            <li><i class="fa fa-envelope"></i> <span>info@mololongo.com</span></li>
                        </ul>
                    </div>
                    <h2 class="mt-4">Apartman Tanja</h2>
                    <p class="app-desc">
                        Apartman s pogledom na more nalazi se u Ičićima, ulica Mulandovo 3, na prvom katu zgrade. Apartman je površine 60 m2 i pruža ugodan smještaj za 4 odrasle osobe + 1 dijete. Ovaj luksuzni, moderno opremljen apartman sastoji se od sobe s posebno velikim bračnim krevetom, hodnika, blagovaonice, kuhinje, kupaonice i dnevnog boravka s kaučem na razvlačenje. Objekt je tehnički kompletno opremljen što uključuje besplatni WiFi internet, satelitsku televiziju i klima uređaj. Prostrana terasa s koje se pruža predivni pogled na more, kompletno je uređena s elegantnim vanjskim namještajem te je idealan izbor za jutarnji doručak, večernji koktel ili za sunčanje i opuštanje na vanjskoj sofi. Besplatni parking nalazi se u sklopu objekta.
Na temelju nepristranih recenzija, ovo je omiljeni dio odredišta “Opatija” za naše goste s top-lokacijom.
                    </p>
                    <div class="app-meta mb-4">
                        <ul>
                            <li><i class="fa fa-credit-card"></i> </li>
                            <li><i class="fa fa-credit-card-alt"></i> </li>
                        </ul>
                    </div>
                    <div class="app-meta block-meta">
                        <ul>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                        </ul>
                    </div>
                    <div class="sep-text"><span>Sadržaji</span></div>
                    <div class="app-meta grid-meta">
                        <ul>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                        </ul>
                        <ul class="content hideContent">
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                        </ul>
                        <div class="show-more my-4">
                            <a class="button" href="javascript:void(0)">Prikaži više</a>
                        </div>
                        
                    </div>
                    <div class="sep-text"><span>Kalendar raspoloživosti</span></div>
                    <div clasS="app-calendar text-center">
                        <img src="<?php bloginfo('template_directory'); ?>/images/Capture5.png">
                    </div>
                    <div class="sep-text"><span>Virtualna šetnja</span></div>
                    <div clasS="app-virtual text-center">
                        <img src="<?php bloginfo('template_directory'); ?>/images/Capture5.png">
                    </div>
                    
                </div>
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <div class="app-info app-border">
                            <div class="sep-text"><span>Važne informacije</span></div>
                            <div class="app-meta block-meta">
                                <ul>
                                    <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                                    <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                                    <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="app-info app-border">
                            <div class="sep-text"><span>Važne informacije</span></div>
                            <div class="app-meta block-meta">
                                <ul>
                                    <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                                    <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                                    <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="app-price-box">
                    90 €/noć
                </div>
                <div class="book-popup">
                    
                </div>
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sep-text"><span>Važne informacije</span></div>
            </div>
        </div>
        <div class="row no-gutters">
            <div class="col-md-8">
                <div class="app-info app-border">
                    <div class="map-frame">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2805.771374337115!2d14.281301315763171!3d45.31304375153742!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4764a81c585b338d%3A0xc1ef8192255786dc!2sMulandovo%203%2C%2051414%2C%20Opatija!5e0!3m2!1shr!2shr!4v1632485624785!5m2!1shr!2shr" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="app-info app-border">
                    <div class="app-meta block-meta">
                        <ul>
                            <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                            <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                            <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="app-info app-border">
                    <div class="weather">
                        <iframe src="https://www.meteoblue.com/en/weather/widget/daily/opatija_croatia_3194099?geoloc=fixed&days=2&tempunit=CELSIUS&windunit=KILOMETER_PER_HOUR&precipunit=MILLIMETER&coloured=monochrome&pictoicon=0&pictoicon=1&maxtemperature=0&maxtemperature=1&mintemperature=0&mintemperature=1&windspeed=0&windgust=0&winddirection=0&uv=0&humidity=0&precipitation=0&precipitationprobability=0&spot=0&pressure=0&layout=light"  frameborder="0" scrolling="NO" allowtransparency="true" sandbox="allow-same-origin allow-scripts allow-popups allow-popups-to-escape-sandbox" style="width: 108px; height: 420px"></iframe><div><!-- DO NOT REMOVE THIS LINK --><a href="https://www.meteoblue.com/en/weather/week/opatija_croatia_3194099?utm_source=weather_widget&utm_medium=linkus&utm_content=daily&utm_campaign=Weather%2BWidget" target="_blank">meteoblue</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sep-text"><span>Što kažu naši gosti</span></div>
            </div>
        </div>
    </div>
</section>

<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sep-text"><span>Slične smještajne jedinice</span></div>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-2">
                <div class="swiper-wrapper">
                <!-- Slides -->
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <div class="app-box">
        				    <div class="app-image">
        				        <img src="http://dev.mololongo.com/wp-content/uploads/2021/07/apartman-tanja-1-1024x683.jpg">
        				        <span class="app-stars"><i class="fa fa-star"></i></span>
        				    </div>
        				    <div class="app-content">
        				        <div class="app-title">
        				            <h4>Apartman West Wing 1</h4>
        				            <span class="app-price">90 eur/noć</span>
        				        </div>
        				        <div class="app-icons">
        				            <ul class="small-icons">
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				            </ul>
        				        </div>
        				        <div class="app-short">
        				            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        				        </div>
        				        <div class="app-button">
        				            <a href="#" class="button-primary">Rezerviraj</a>
        				        </div>
        				    </div>
        				    
        				</div>
                    </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                    <div class="app-box">
    				    <div class="app-image">
    				        <img src="http://dev.mololongo.com/wp-content/uploads/2021/07/apartman-tanja-1-1024x683.jpg">
    				        <span class="app-stars"><i class="fa fa-star"></i></span>
    				    </div>
    				    <div class="app-content">
    				        <div class="app-title">
    				            <h4>Apartman West Wing 2</h4>
    				            <span class="app-price">90 eur/noć</span>
    				        </div>
    				        <div class="app-icons">
    				            <ul class="small-icons">
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				            </ul>
    				        </div>
    				        <div class="app-short">
    				            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
    				        </div>
    				        <div class="app-button">
    				            <a href="#" class="button-primary">Rezerviraj</a>
    				        </div>
    				    </div>
    				    
    				</div>
                </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                    <div class="app-box">
    				    <div class="app-image">
    				        <img src="http://dev.mololongo.com/wp-content/uploads/2021/07/apartman-tanja-1-1024x683.jpg">
    				        <span class="app-stars"><i class="fa fa-star"></i></span>
    				    </div>
    				    <div class="app-content">
    				        <div class="app-title">
    				            <h4>Apartman West Wing 3</h4>
    				            <span class="app-price">90 eur/noć</span>
    				        </div>
    				        <div class="app-icons">
    				            <ul class="small-icons">
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				                <li><i class="fa fa-plane"></i> Crikvenica</li>
    				            </ul>
    				        </div>
    				        <div class="app-short">
    				            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
    				        </div>
    				        <div class="app-button">
    				            <a href="#" class="button-primary">Rezerviraj</a>
    				        </div>
    				    </div>
    				    
    				</div>
                </div>
                </div>
                <div class="swiper-slide">
                    <div class="col-md-12">
                        <div class="app-box">
        				    <div class="app-image">
        				        <img src="http://dev.mololongo.com/wp-content/uploads/2021/07/apartman-tanja-1-1024x683.jpg">
        				        <span class="app-stars"><i class="fa fa-star"></i></span>
        				    </div>
        				    <div class="app-content">
        				        <div class="app-title">
        				            <h4>Apartman West Wing 4</h4>
        				            <span class="app-price">90 eur/noć</span>
        				        </div>
        				        <div class="app-icons">
        				            <ul class="small-icons">
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				                <li><i class="fa fa-plane"></i> Crikvenica</li>
        				            </ul>
        				        </div>
        				        <div class="app-short">
        				            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</p>
        				        </div>
        				        <div class="app-button">
        				            <a href="#" class="button-primary">Rezerviraj</a>
        				        </div>
        				    </div>
        				    
        				</div>
                    </div>
                </div>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>

<section class="parallax-cta" style="background: url('https://dev.mololongo.com/wp-content/uploads/2021/07/3d-rendering-dining-bar-in-small-villa-near-beautiful-beach-and-sea-at-noon-with-blue-sky.jpg') ;    background-position: 0px -182px;
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center text-white">
                <h4 class="mb-3">Trebate li i automobil?</h4>
                <p class="pb-2">Obratite nam se s povjerenjem!</p>
                <a href="#" class="button-color">Saznaj više</a>
            </div>
            
        </div>
    </div>
</section>
<section class="contact-office">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h5>Naši uredi</h5>
            </div>
            <div class="col-md-3">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6>Rijeka</h6>
                    </div>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"></i>Trpimirova 1A</li>
                        <li><i class="fa fa-map-marker"></i>+385 51 452 883</li>
                        <li><i class="fa fa-map-marker"></i>opatija@mololongo.com</li>
                        <li><i class="fa fa-map-marker"></i>Promjenjivo</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6>Opatija</h6>
                    </div>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"></i>Trpimirova 1A</li>
                        <li><i class="fa fa-map-marker"></i>+385 51 452 883</li>
                        <li><i class="fa fa-map-marker"></i>opatija@mololongo.com</li>
                        <li><i class="fa fa-map-marker"></i>Promjenjivo</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6>Otok Krk - Punat</h6>
                    </div>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"></i>Trpimirova 1A</li>
                        <li><i class="fa fa-map-marker"></i>+385 51 452 883</li>
                        <li><i class="fa fa-map-marker"></i>opatija@mololongo.com</li>
                        <li><i class="fa fa-map-marker"></i>Promjenjivo</li>
                    </ul>
                </div>
            </div>
            <div class="col-md-3">
                <div class="contact-box">
                    <div class="contact-title">
                        <h6>Crikvenica</h6>
                    </div>
                    <ul class="contact-list">
                        <li><i class="fa fa-map-marker"></i>Trpimirova 1A</li>
                        <li><i class="fa fa-map-marker"></i>+385 51 452 883</li>
                        <li><i class="fa fa-map-marker"></i>opatija@mololongo.com</li>
                        <li><i class="fa fa-map-marker"></i>Promjenjivo</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>


<?php
get_footer();
