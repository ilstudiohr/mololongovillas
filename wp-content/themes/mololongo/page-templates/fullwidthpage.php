<?php
/**
 * Template Name: Full Width Page
 *
 * Template for displaying a page without sidebar even if a sidebar widget is published.
 *
 * @package MoloLongo
 */

// Exit if accessed directly.
defined( 'ABSPATH' ) || exit;

get_header();
$container = get_theme_mod( 'mololongo_container_type' );

if ( is_front_page() ) {
	get_template_part( 'global-templates/hero' );
}
?>
<?php $backgroundImg = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' ); ?>
<section class="small-hero" style="background: url('<?php echo $backgroundImg[0]; ?>') ;background-position: center;
    background-repeat: no-repeat;
    background-size: cover;">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <h1><?php the_title();?></h1>
                <?php if( get_field('intro_text') ): ?>
                	<p class="small-desc"><?php the_field('intro_text'); ?></p>
                <?php endif; ?>
                
            </div>
        </div>
    </div>
</section>
<?php if( have_rows('regija_1') ): while ( have_rows('regija_1') ) : the_row(); ?>
<section style="background: url('<?php echo esc_url(get_template_directory_uri()); ?>/images/Dots6.png') ;background-position: center left;background-repeat: no-repeat;">
    
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2><?php the_sub_field('naslov'); ?></h2>
                <p><?php the_sub_field('uvodni_tekst'); ?></p>
                <div class="section-icon">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Kreveti', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_1'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_1'); ?></p>
                    </div>
                </div>
                <div class="section-icon mt-3">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Kreveti', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_2'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_2'); ?></p>
                    </div>
                </div>
                <div class="text-center mt-4">
                    <a href="/accommodation" class="button-primary"><?php _e('Pretraži vile', 'molonew'); ?></a>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="img-shape_lt text-center">
                    <?php 
                    $image = get_sub_field('slika');
                    if( !empty( $image ) ): ?>
                        <img class="mw400" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <div class="section-icon mt-5">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_3'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_3'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<?php endwhile; endif; ?>
<section class="sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>
<?php if( have_rows('regija_2') ): while ( have_rows('regija_2') ) : the_row(); ?>
<section style="background: url('<?php echo esc_url(get_template_directory_uri()); ?>/images/Dots6.png') ;background-position: center left;background-repeat: no-repeat;">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <h2><?php the_sub_field('naslov'); ?></h2>
                <p><?php the_sub_field('uvodni_tekst'); ?></p>
                <div class="img-shape_lt text-center mt-3">
                    <?php 
                    $image2 = get_sub_field('slika2');
                    if( !empty( $image2 ) ): ?>
                        <img  class="mw400" src="<?php echo esc_url($image2['url']); ?>" alt="<?php echo esc_attr($image2['alt']); ?>" />
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="section-icon">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_1'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_1'); ?></p>
                    </div>
                </div>
                <div class="section-icon mt-3">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_2'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_2'); ?></p>
                    </div>
                </div>
                <div class="section-icon mt-3">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_3'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_3'); ?></p>
                    </div>
                </div>
                <div class="text-center mt-4">
                    <a href="/accommodation" class="button-primary"><?php _e('Pretraži vile', 'molonew'); ?></a>
                </div>
            </div>
        </div>
    </div>
    
</section>
<?php endwhile; endif; ?>
<section class="sep">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="separator"></div>
            </div>
        </div>
    </div>
</section>
<?php if( have_rows('regija_3') ): while ( have_rows('regija_3') ) : the_row(); ?>
<section style="background: url('<?php echo esc_url(get_template_directory_uri()); ?>/images/Dots6.png') ;background-position: center left;background-repeat: no-repeat;">
    
    <div class="container">
        
        <div class="row">
            <div class="col-md-6">
                <h2><?php the_sub_field('naslov'); ?></h2>
                <p><?php the_sub_field('uvodni_tekst'); ?></p>
                <div class="section-icon">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_1'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_1'); ?></p>
                    </div>
                </div>
                <div class="section-icon mt-3">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_2'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_2'); ?></p>
                    </div>
                </div>
                <div class="text-center mt-4">
                    <a href="/accommodation" class="button-primary"><?php _e('Pretraži vile', 'molonew'); ?></a>
                </div>
                
            </div>
            <div class="col-md-6">
                <div class="img-shape_lt text-center">
                    <?php 
                    $image = get_sub_field('slika');
                    if( !empty( $image ) ): ?>
                        <img class="mw400" src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    <?php endif; ?>
                </div>
                <div class="section-icon mt-5">
                    <div class="icon-section">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="<?php _e('Krevet', 'molonew'); ?>">
                    </div>
                    <div class="content-section">
                        <h4><?php the_sub_field('naslov_sekcije_3'); ?></h4>
                        <p><?php the_sub_field('tekst_sekcije_3'); ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</section>
<?php endwhile; endif; ?>
<?php if( get_field('pozadinska_slika')  ): ?>
<section class="parallax-cta" style="background: url('<?php the_field('pozadinska_slika'); ?>') ; background-repeat: no-repeat;background-size: cover;background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row align-center">
            <div class="col-md-6">
                <h5 class="text-left mb-4"><?php _e('Spremni? Idemo.', 'molonew'); ?></h5>
                <a href="<?php _e('/kontakt', 'molonew'); ?>" class="button-color"><?php _e('Kontakt', 'molonew'); ?></a>
            </div>
            <div class="col-md-6">
                
            </div>
        </div>
    </div>
</section>
<?php endif; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
					while ( have_posts() ) {
						the_post();
						the_content();
					}
					?>
            </div>
        </div>
    </div>
</section>
<?php if( get_field('slika_lijevo') || get_field('tekst_desno') ): ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php 
                $image = get_field('slika_lijevo');
                if( !empty( $image ) ): ?>
                    <div class="round-img h350">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </div>
                <?php endif; ?>
                
            </div>
            <div class="col-md-6">
                <?php if( get_field('tekst_desno') ): ?>
                	<p class="small-desc"><?php the_field('tekst_desno'); ?></p>
                <?php endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('slika_desno') || get_field('tekst_lijevo_2') ): ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <?php if( get_field('tekst_lijevo_2') ): ?>
                	<p class="small-desc"><?php the_field('tekst_lijevo_2'); ?></p>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <?php 
                $image = get_field('slika_desno');
                if( !empty( $image ) ): ?>
                    <div class="round-img h350">
                        <img src="<?php echo esc_url($image['url']); ?>" alt="<?php echo esc_attr($image['alt']); ?>" />
                    </div>
                <?php endif; ?>
                
            </div>
            
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('tekst_lijevo') ||  get_field('iframe_desno') ): ?>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="info-box text-right">
                    <?php if( get_field('tekst_lijevo') ): ?>
                    	<p class="small-desc"><?php the_field('tekst_lijevo'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-md-6">
                <div class="map-position">
                    <?php if( get_field('iframe_desno') ): ?>
                    	<p class="small-desc"><?php the_field('iframe_desno'); ?></p>
                    <?php endif; ?>
                </div>
            </div>
            
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( have_rows('slika_1')): ?>
<section class="image-boxes">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <?php if( have_rows('slika_1') ): while ( have_rows('slika_1') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_1'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_1'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_1'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_2') ): while ( have_rows('slika_2') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_2'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_2'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_2'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_3') ): while ( have_rows('slika_3') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_3'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_3'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_3'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-3">
                <?php if( have_rows('slika_4') ): while ( have_rows('slika_4') ) : the_row(); ?>
                <div class="single-image-box">
                    <div class="single-image-box_bg" style="background: url('<?php the_sub_field('pozadinska_slika_4'); ?>') ;background-position: center;background-repeat: no-repeat;background-size: cover;"><div class="overlay"></div></div>
                    <div class="single-image-box_content">
                        <h5><?php the_sub_field('naslov_slike_4'); ?></h5>
                        <p><?php the_sub_field('tekst_slike_4'); ?></p>
                    </div>
                    
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field('naslov_sekcije')  ): ?>
<section class="help-box">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php if( get_field('naslov_sekcije') ): ?>
                	<h3 class="text-center"><?php the_field('naslov_sekcije'); ?></h3>
                <?php endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_1') ): while ( have_rows('box_1') ) : the_row(); ?>
                <div class="help_sinlge-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_1') ): ?>
                            <img src="<?php the_sub_field('ikona_1'); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><h4><?php the_sub_field('naslov_kucice_1'); ?></h4></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_2') ): while ( have_rows('box_2') ) : the_row(); ?>
                <div class="help_sinlge-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_2') ): ?>
                            <img src="<?php the_sub_field('ikona_2'); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><h4><?php the_sub_field('naslov_kucice_2'); ?></h4></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
            <div class="col-md-4">
                <?php if( have_rows('box_3') ): while ( have_rows('box_3') ) : the_row(); ?>
                <div class="help_sinlge-box invert-box">
                    <div class="help_sinlge-box-icon">
                        <?php if( get_sub_field('ikona_3') ): ?>
                            <img src="<?php the_sub_field('ikona_3'); ?>" />
                        <?php endif; ?>
                    </div>
                    <div class="help_sinlge-box-title"><h4><?php the_sub_field('naslov_kucice_3'); ?></h4></div>
                </div>
                <?php endwhile; endif; ?>
            </div>
        </div>
    </div>
</section>
<?php endif; ?>
<?php if( get_field( 'car' )): ?>
<section class="pricelist">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="pricelist_content">
                    
                    <a class="price_btn price_btn1 non_active_btn"><span></span><?php _e('Prikaži cijene van sezone', 'molonew'); ?></a>
                
                    <div class="high_season">
                        <ul class="tabs nav nav-tabs">
                            <li class="tab_hrk active"><a href="#tab1" data-toggle="tab">HRK</a></li>
                            <li class="tab_eur"><a href="#tab2" data-toggle="tab">EUR</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab tab-pane active" id="tab1">
                            <div class="block car_row">
                                <span class="car"><?php _e('Model automobila', 'molonew'); ?></span>
                                
                                <span class="car_info"><?php _e('Opis', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('1-2 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('3-6 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('7-13 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('14+ dana', 'molonew'); ?></span>
                            </div>
                
                            <?php if( have_rows('car') ): while ( have_rows('car') ) : the_row(); ?><div class="block car_row">
                                <span class="car">
                                    <img src="<?php the_sub_field('image'); ?>" alt="" />
                                    <strong><?php the_sub_field('title'); ?></strong>
                                </span>
                                <span class="car_info"><?php the_sub_field('description'); ?></span>
                                <span class="price"><?php the_sub_field('price-a1'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-a2'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-a3'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-a4'); ?> kn</span>
                            </div><?php endwhile; endif; ?>	
                        	
                        </div>
                
                        <div class="tab tab-pane" id="tab2">
                            <div class="block car_row">
                                <span class="car"><?php _e('Model automobila', 'molonew'); ?></span>
                                
                                <span class="car_info"><?php _e('Opis', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('1-2 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('3-6 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('7-13 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('14+ dana', 'molonew'); ?></span>
                            </div>
                
                            <?php if( have_rows('car') ): while ( have_rows('car') ) : the_row(); ?><div class="block car_row">
                                <span class="car">
                                    <img src="<?php the_sub_field('image'); ?>" alt="" />
                                    <strong><?php the_sub_field('title'); ?></strong>
                                </span>
                                <span class="car_info"><?php the_sub_field('description'); ?></span>
                                <span class="price"><?php the_sub_field('price-a1-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-a2-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-a3-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-a4-e'); ?> €</span>
                            </div><?php endwhile; endif; ?>	
                            
                        </div>
                        </div>
                        
                    </div>
                    
                    <!-- Low Season -->
                    <div class="low_season">
                        <ul class="tabs2 nav nav-tabs">
                            <li class="tab_hrk active"><a href="#tab3" data-toggle="tab">HRK</a></li>
                            <li class="tab_eur"><a href="#tab4" data-toggle="tab">EUR</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab tab-pane active" id="tab3">
                            <div class="block car_row">
                                <span class="car"><?php _e('Model automobila', 'molonew'); ?></span>
                                
                                <span class="car_info"><?php _e('Opis', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('1-2 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('3-6 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('7-13 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('14+ dana', 'molonew'); ?></span>
                            </div>
                
                            <?php if( have_rows('car') ): while ( have_rows('car') ) : the_row(); ?><div class="block car_row">
                                <span class="car">
                                    <img src="<?php the_sub_field('image'); ?>" alt="" />
                                    <strong><?php the_sub_field('title'); ?></strong>
                                </span>
                                <span class="car_info"><?php the_sub_field('description'); ?></span>
                                <span class="price"><?php the_sub_field('price-b1'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-b2'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-b3'); ?> kn</span>
                                <span class="price"><?php the_sub_field('price-b4'); ?> kn</span>
                            </div><?php endwhile; endif; ?>	
                            
                        </div>
                
                        <div class="tab tab-pane" id="tab4">
                            <div class="block car_row">
                                <span class="car"><?php _e('Model automobila', 'molonew'); ?></span>
                                
                                <span class="car_info"><?php _e('Opis', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('1-2 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('3-6 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('7-13 dana', 'molonew'); ?></span>
                                
                                <span class="price"><?php _e('14+ dana', 'molonew'); ?></span>
                            </div>
                
                            <?php if( have_rows('car') ): while ( have_rows('car') ) : the_row(); ?><div class="block car_row">
                                <span class="car">
                                    <img src="<?php the_sub_field('image'); ?>" alt="" />
                                    <strong><?php the_sub_field('title'); ?></strong>
                                </span>
                                <span class="car_info"><?php the_sub_field('description'); ?></span>
                                <span class="price"><?php the_sub_field('price-b1-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-b2-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-b3-e'); ?> €</span>
                                <span class="price"><?php the_sub_field('price-b4-e'); ?> €</span>
                            </div><?php endwhile; endif; ?>	
                        </div>
                        </div>
                
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>  
</section>
<?php endif; ?>

<?php
get_footer();
