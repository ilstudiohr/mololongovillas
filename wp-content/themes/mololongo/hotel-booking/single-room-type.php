<?php
if ( !defined( 'ABSPATH' ) ) {
	exit;
}
?>

<?php get_header(); ?>	

<section class="app-top">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-5">
                <div class="apartment-title">
                    <h1><?php the_title(); ?></h1>
                    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                    <span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
                    
                </div>
                <div class="app-meta">
                    <ul>
                        <?php if( get_sub_field('beds') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="beds">
                            <span><?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?></span>
                        </li>
                        <?php endif; ?> 
                        <?php if( get_sub_field('basic_capacity') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>">
                            <span><?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></span>
                        </li>
                        <?php endif; ?> 
                        <?php if( get_sub_field('space') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_villa_size.svg" alt="<?php _e('Soba', 'molonew'); ?>">
                            <span><?php the_sub_field('space'); ?>m<sup>2</sup></span>
                        </li>
                        <?php endif; ?>  
                        
                        <?php if( get_sub_field('bathrooms') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bathroom.svg" alt="<?php _e('Kupaonica', 'molonew'); ?>">
                            <span><?php the_sub_field('bathrooms'); ?></span>
                        </li>
                        <?php endif; ?>
                        <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); if( get_sub_field('parking') ): ?>	
                        <li>
                            <p><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg" /></p>
                            <span><?php _e('Parking', 'molonew'); ?></span>
                        </li>
                        <?php endif; endwhile; endif; ?>
                    </ul>
                    <?php endwhile; endif; ?>
                    
                </div>
            </div>
            
            <div class="col-md-8">
                <div class="app_feat-img">
                    <?php 
                    $images = get_field('room_slider');
                        if($images): ?>
                        <div class="gallery">   
                            <?php $i=0; foreach( $images as $image ) : ?>
                              <a href="<?php echo $image['url']; ?>" target="_blank" rel="lightbox" data-lightbox="gallery">
                                <?php if( $i==0 ) : ?>
                                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                <?php endif; ?>
                              </a>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php endif; ?>
                    
                </div>
            </div>
            <div class="col-md-4">
                <div class="app-gallery">
                    <?php $images = get_field('room_slider'); if ($images) { $counter = 1; ?>
                        <?php foreach( $images as $image ) { ?>	
                        	<div class="gallery-side">
                                <a href="<?php echo $image['url']; ?>" rel="lightbox" data-lightbox="gallery">
                                     <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>
                            </div>
                        <?php $counter++; if ($counter == 5) { break; }} ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="row app-content mt-2">
            <div class="col-md-9">
                <div class="app-info app-border">
                    <div class="app-meta block-meta">
                        <ul>
                            <li><i class="fa fa-phone"></i> <span>+385 91 600 6646</span></li>
                            <li><i class="fa fa-envelope"></i> <span>info@mololongo.com</span></li>
                        </ul>
                    </div>
                    <h2 class="mt-4"><?php the_title();?></h2>
                    <p class="app-desc">
                        <?php the_content();?>
                    </p>
                    <div class="app-meta mb-4">
                        <ul>
                            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/c-master.svg" alt="<?php _e('Master', 'molonew'); ?>"> </li>
                            <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/c-visa.svg" alt="<?php _e('Visa', 'molonew'); ?>"> </li>
                        </ul>
                    </div>
                    <div class="app-meta block-meta">
                        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                        <ul>
                            <li>
                                <?php if( get_sub_field('price') ): ?>
                                <i class="fa fa-euro"></i><span><?php _e('od', 'molonew'); ?>  <?php the_sub_field('price') ?> €</span>
                                <?php endif; ?>
                            </li>
                            
                            <?php if( get_sub_field('address') ): ?>
                            <li>
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pin.svg" alt="room">
                                <span><?php the_sub_field('address'); if( get_sub_field('city') ): ?>, <?php the_sub_field('city'); endif; ?>	</span>
                            </li>
                            <?php endif; ?>  
                            <?php if( get_sub_field('check_1') ): ?>
                            <li>
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_clock.svg" />
                                <span>Check-in: <?php the_sub_field('check_1'); ?> Check-out: <?php the_sub_field('check_out'); ?></span>
                            </li>
                            <?php endif; ?>	
                            
                        </ul>
                        <?php endwhile; endif; ?>
                    </div>
                    
                    <?php do_action( 'mphb_render_single_room_type_before_content' ); ?>
        
                    <?php
                    /**
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderTitle				- 10
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderFeaturedImage		- 20
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderDescription		- 30
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderPrice				- 40
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderAttributes			- 50
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderCalendar			- 60
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderReservationForm	- 70
                     */
                    do_action( 'mphb_render_single_room_type_content' );
                    ?>
            
                    <?php do_action( 'mphb_render_single_room_type_after_content' ); ?>
                    
                    <div class="sep-text"><span><?php _e('Kalendar raspoloživosti', 'molonew'); ?></span></div>
                    <div class="app-calendar text-center">
                        <?php echo do_shortcode("[mphb_availability_calendar monthstoshow='4']"); ?> 
                    </div>
                    <div class="sep-text"><span><?php _e('Virtualna šetnja', 'molonew'); ?></span></div>
                    <div clasS="app-virtual text-center">
                        <img src="<?php bloginfo('template_directory'); ?>/images/Capture5.png">
                    </div>
                </div>
                <div class="row no-gutters">
                    <div class="col-md-6">
                        <div class="app-info app-border">
                            <div class="sep-text"><span>Važne informacije</span></div>
                            <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                <div class="app-meta block-meta">
                                    <ul>
                                        <?php if( get_sub_field('concierge') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_doorman.svg"/>
                                        	<span><?php _e('Concierge / vratar (svakodnevno od 9:00 do 22:00)', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?> 
                                        <?php if( get_sub_field('scooter') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_scooter.svg"/>
                                        	<span><?php _e('Najam električnih skutera', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('self') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-self.svg"/>
                                        	<span><?php _e('Samostalna prijava', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('airport') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airport.svg"/>
                                        	<span><?php _e('Rezervirajte uslugu prijevoza do/od aerodroma', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('trips') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_hiking.svg"/>
                                        	<span><?php _e('Rezervirajte izlete i obilaske', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?>
                                        <?php if( get_sub_field('breakfast') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_coffee.svg"/>
                                        	<span><?php _e('Doručak (na zahtjev, uz nadoplatu)', 'molonew'); ?></span>
                                        </li>
                                        <?php endif; ?>
                                    </ul>
                                </div>
                            
                            <?php endwhile; endif; ?>
                            
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="app-info app-border">
                            <div class="sep-text"><span><?php _e('Sigurnost', 'molonew'); ?>"</span></div>
                            <div class="app-meta block-meta">
                                <ul>
                                    <li><i class="fa fa-euro"></i> <span>90 €</span></li>
                                    <li><i class="fa fa-map-marker"></i> <span>Mulandovo 3, Ičići, 1. kat (bez lifta)</span></li>
                                    <li><i class="fa fa-key"></i> <span>Check in: 17:00, Check out: 10:00</span></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div  id="sidebar">
                    <div class="sidebar__inner">
                    <div class="app-price-box">
                    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                        <?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?>
                    <?php endwhile; endif; ?>
                    </div>
                    <div class="book-popup app-price-box mt-4">
                        <form class="booknow-form">
                            <div class="sidebar-input">
                                <label>Check-in Date</label>
                                <input type="text" placeholder="Check-in Date"/>
                            </div>
                            <div class="sidebar-input">
                                <label>Check-out Date</label>
                                <input type="text" placeholder="Check-out Date"/>
                            </div>
                            <div class="sidebar-input">
                                <label>Odrasli</label>
                                    <select class="sidebar-select">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                            <div class="sidebar-input">
                                <label>Djeca</label>
                                    <select class="sidebar-select">
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </div>
                        </form>
                    </div>
                </div>
                </div>
                
                
            </div>
        </div>
    </div>
</section>
<section class="map-section">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sep-text"><span><?php _e('Pronađite na karti', 'molonew'); ?></span></div>
            </div>
        </div>
        <div class="row no-gutters flex-row">
            <div class="col-md-8 eq-h">
                <div class="app-info app-border">
                    <div class="map-frame mb-4">
                        <div id="dd_map"></div>
                    </div>
                    <a href="https://www.viamichelin.com/" class="button-primary"><?php _e('Isplanirajte putovanje', 'molonew'); ?></a>
                </div>
            </div>
            <div class="col-md-2 eq-h">
                <div class="app-info app-border">
                    <div class="app-meta block-meta">
                        <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                        <ul>
                            <?php if( get_sub_field('to_beach') ): ?>
                            <li>
                            	<div class="img_box"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_beach.svg"/>
                            	<span><?php _e('Plaža', 'molonew'); ?> <?php the_sub_field('to_beach'); ?>
                            </li>
                            <?php endif; ?>   
                            
                            <?php if( get_sub_field('to_restaurant') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-pribor.svg"/>
                            	<span><?php _e('Restoran', 'molonew'); ?> <?php the_sub_field('to_restaurant'); ?>
                            </li>
                            <?php endif; ?>   
                            
                            <?php if( get_sub_field('to_atm') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_atm.svg"/>
                            	<span><?php _e('Bankomat', 'molonew'); ?> <?php the_sub_field('to_atm'); ?>
                            </li>
                            <?php endif; ?>   
                            
                            <?php if( get_sub_field('to_parking') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg"/>
                            	<span><?php _e('Parking', 'molonew'); ?> <?php the_sub_field('to_parking'); ?>
                            </li>
                            <?php endif; ?>   
                            
                            <?php if( get_sub_field('to_airport') ): ?>
                            <li>
                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airport.svg"/>
                            	<span><?php _e('Aerodrom', 'molonew'); ?> <?php the_sub_field('to_airport'); ?>
                            </li>
                            <?php endif; ?> 
                        </ul>
                        <?php endwhile; endif; ?>
                    </div>
                </div>
            </div>
            <div class="col-md-2 eq-h">
                <div class="app-info app-border">
                    <div class="weather">
                        <div class="app-meta block-meta">
                        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                            <?php if( get_sub_field('address') ): ?>
                                <span><?php if( get_sub_field('city') ): ?> <?php the_sub_field('city'); endif; ?>	</span>
                            <?php endif; ?>  
                        <?php endwhile; endif; ?>
                    </div>
                        <div class="app"> 
                            <div id="temp-main">0°</div>
                            <div id="condition">unknown</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="testimonials">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="sep-text"><span><?php _e('Što kažu naši gosti', 'molonew'); ?></span></div>
                <?php echo do_shortcode('[sp_testimonial id="853319"]'); ?>
            </div>
        </div>
    </div>
</section>
<section class="app-list pt-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="sep-text"><span><?php _e('Slične smještajne jedinice', 'molonew'); ?></span></div>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4">
                <div class="swiper-wrapper">
                <?php 
                $terms = get_the_terms( $post->ID , 'mphb_room_type_category', 'string');
                $term_ids = wp_list_pluck($terms,'term_id');
                $args = array('posts_per_page' => 5, 'orderby' => 'menu_order', 'order' => 'ASC','post_type' => 'mphb_room_type','tax_query' => array(array('taxonomy' => 'mphb_room_type_category', 'field' => 'id','terms' => $term_ids, 'operator'=> 'IN'))); $new = new WP_Query( $args ); if ( have_posts() ) while ($new->have_posts()) : $new->the_post(); ?>
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <?php the_post_thumbnail('medium'); ?>
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				    </div>
            				    <div class="app-content">
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				            <span class="app-price"><?php the_sub_field('price'); ?> <?php _e('eur/noć', 'molonew'); ?></span>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('basic_capacity') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> + <?php the_sub_field('spare_capacity'); endif; ?></li>
            				                <?php endif; ?>  
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(30); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				    </div>
            				    
            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); ?>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
</section>
<section class="parallax-cta" style="background: url('https://dev.mololongo.com/wp-content/uploads/2021/07/3d-rendering-dining-bar-in-small-villa-near-beautiful-beach-and-sea-at-noon-with-blue-sky.jpg') ;    background-position: 0px -182px;
    background-repeat: no-repeat;
    background-size: cover;
    background-attachment:fixed">
    <div class="parallax-overlay"></div>
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center text-white">
                <h4 class="mb-3"><?php _e('Trebate li i automobil?', 'molonew'); ?></h4>
                <p class="pb-2"><?php _e('Obratite nam se s povjerenjem!', 'molonew'); ?></p>
                <a href="/kontakt" class="button-color"><?php _e('Saznaj više', 'molonew'); ?></a>
            </div>
            
        </div>
    </div>
</section>
<?php require 'offices.php';?>

<?php

/**

 * @hooked \MPHB\Views\SingleRoomTypeView::renderPageWrapperEnd - 10

 */

do_action( 'mphb_render_single_room_type_wrapper_end' );

?>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB5iZ_Se_RALfitHR9bIhpctdD52oGRG-w&libraries=places"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/markerclusterer.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/sticky-sidebar.min.js"></script>

<script>
    function initMap(){
          // map options
          var options = {
            zoom:10,
            center:{<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?><?php endwhile; endif; ?>},
			mapTypeId: "roadmap",
    		// disableDefaultUI: true,
			styles: [

				{
					featureType: "all",
					elementType: "all",
					stylers: [
					  { "saturation": -100 }
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{ "saturation": -100 },
						{ "lightness": 65 },
						{ "visibility": "on" }
					]
				},				
				
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{ "hue": "#ffff00" },
						{ "lightness": -10 },
						{ "saturation": -100 }
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{ "lightness": -25 },
						{ "saturation": -100 }
					]
				}
			]
          }
    var map = new google.maps.Map(document.getElementById('dd_map'),options);

          var markers = [
				<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
						<?php if( get_sub_field('latitude') ): ?>{
						  coords:{lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?>},
						  iconImage:'<?php echo esc_url(get_template_directory_uri()); ?>/images/map_pin.png',
						  content:
							'<p class="dd_map_title"><?php the_title(); ?></p>' +
							'<p class="dd_map_p"><span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span></p>' +
							
							'<p class="dd_map_p"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.png" /><?php the_sub_field('address'); ?>, <?php the_sub_field('city'); ?></p>' +
							
							'<p class="dd_map_p"><span class="dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/map_guests.png" /><?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> &#43; <?php the_sub_field('spare_capacity'); endif; ?></span><span class="dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/map_beds.png" /><?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?></span></p>' +
							'<p class="dd_map_p">Cijena od: <strong><?php the_sub_field('price'); ?> € <small>za 3 noći</small></strong></p>' +
							'<a class="button dd_map_button dd_map_button2" target="_blank" href="<?php the_sub_field('google_map'); ?>" target="_blank">Pogledaj na karti</a>'
						},<?php endif; ?>
				<?php endwhile; endif; ?>	
          ];
          // Loop through markers
          var gmarkers = [];
          for(var i = 0; i < markers.length; i++){
            gmarkers.push(addMarker(markers[i]));

          }

          //Add MArker function
          function addMarker(props){
            var marker = new google.maps.Marker({
              position:props.coords,
              map:map,

            });

            if(props.iconImage){
              marker.setIcon(props.iconImage);
            }

            //Check content
            if(props.content){
              var infoWindow = new google.maps.InfoWindow({
                content:props.content
              });
              marker.addListener('click',function(){
                infoWindow.open(map,marker);
              });
            }
            return marker;
          }
		  
        var markerCluster = new MarkerClusterer(map, gmarkers, 
          {
            // for default marker sizes (m1, m2, m3...)
			//imagePath: '<?php echo esc_url(get_template_directory_uri()); ?>/images/m'
			
			styles:[{
			
				url: "<?php echo esc_url(get_template_directory_uri()); ?>/images/m5.png",
					width: 90,
					height:90,
					fontFamily:"Nunito Sans",
					textSize:16,
					textColor:"#ffffff",
					//color: #00FF00,
			}]			
			
          });
		  
        }
    google.maps.event.addDomListener(window, 'load', initMap);
</script>
<script>
    jQuery(function($){
    function getLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(getWeather);
        }else{
            alert("Geolocation not supported by this browser");
        }
    }
    function getWeather(position){
        let lat = '45.337620';
        let long = '14.305196';
        let API_KEY = '2398e46ce4a606d494453c5342cc0b97';
        let baseURL = `https://api.openweathermap.org/data/2.5/onecall?&lat=${lat}&lon=${long}&appid=${API_KEY}`;
        
        

        $.get(baseURL,function(res){
            let data = res.current;
            let temp = Math.floor(data.temp - 273);
            let condition = data.weather[0].description;

            $('#temp-main').html(`${temp}°`);
            $('#condition').html(condition);
        })
        
    }

    getLocation();
})
</script>
<?php get_footer(''); ?>