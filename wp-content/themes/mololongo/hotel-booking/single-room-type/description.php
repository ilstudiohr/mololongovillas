<?php



if ( !defined( 'ABSPATH' ) ) {

	exit;

}

?>

    <div class="app-info border-top border-bottom"  id="content">
        <h4 class="my-5"><?php _e('Sadržaji', 'molonew'); ?></h4>
    <?php
        $counter = '0';
    ;?>
        <div class="app-meta grid-meta">
        
        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
            <ul class="amenities_content single-app four-col">
                <?php if( get_sub_field('povrsina_ville') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_villa_size.svg"/>
                    <span><?php _e('Površina vile:', 'molonew'); ?> <?php the_sub_field('povrsina_ville'); ?> m<sup>2</sup></span>
                </li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('povrsina_zemljista') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_garden_size.svg"/>
                    <span><?php _e('Površina zemljišta:', 'molonew'); ?> <?php the_sub_field('povrsina_zemljista'); ?> m<sup>2</sup></span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('year') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_crane.svg"/>
                    <span><?php _e('Godina gradnje:', 'molonew'); ?> <?php the_sub_field('year'); ?> m<sup>2</sup></span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('tip_kuce') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_crane.svg"/>
                    <span><?php _e('Tip kuće:', 'molonew'); ?> <?php the_sub_field('tip_kuce'); ?></span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('gosti') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests.svg"/>
                    <span><?php _e('Broj gostiju optimalno:', 'molonew'); ?> <?php the_sub_field('gosti'); ?> </span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('gosti_max') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests.svg"/>
                    <span><?php _e('Broj gostiju max:', 'molonew'); ?> <?php the_sub_field('gosti_max'); ?></span>
                </li>
                <?php endif; ?>

                <?php if( get_sub_field('bathrooms') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bathroom.svg"/>
                    <span><?php _e('Broj kupaona:', 'molonew'); ?> <?php the_sub_field('bathrooms'); ?></span>
                </li>
                <?php endif; ?>
                <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                <?php if( get_sub_field('parking') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_parking.svg"/>
                        <span><?php _e('Parking', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('wifi') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_wifi.svg"/>
                        <span><?php _e('Wifi', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('ograda') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_fance.svg"/>
                        <span><?php _e('Potpuno ograđeno', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('rostilj') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-barbercue.svg"/>
                        <span><?php _e('Vanjski roštilj', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('sauna') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_sauna.svg"/>
                        <span><?php _e('Sauna', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('sattv') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon-tv.svg"/>
                        <span><?php _e('Satelitska/kabelska TV', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('bazen_hot') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool_hot.svg"/>
                        <span><?php _e('Grijani bazen', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('bazen_unutarnji') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool_inside.svg"/>
                        <span><?php _e('Unutarnji bazen', 'molonew'); ?></span>
                    </li>
                <?php endif; ?>
                   <!-- <?php if( get_sub_field('centralno') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_central_heating.svg"/>
                            <span><?php _e('Centralno grijanje', 'molonew'); ?> </span>
                        </li>
                    <?php endif; ?> -->
                    <?php if( get_sub_field('klima_grijanje') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airco_hot.svg"/>
                            <span><?php _e('Grijanje klima uređajima', 'molonew'); ?> </span>
                        </li>
                    <?php endif; ?>
                    <?php if( get_sub_field('klima_broj') ): ?>
                        <li>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airco_cold.svg"/>
                            <span><?php _e('Broj klima uređaja:', 'molonew'); ?> <?php the_sub_field('klima_broj'); ?></span>
                        </li>
                    <?php endif; ?>
                    <?php if( get_sub_field('lezaljke_broj') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_sunbed.svg"/>
                    <span><?php _e('Ležaljke za sunčanje', 'molonew'); ?> <?php the_sub_field('lezaljke_broj'); ?></span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('bazen') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pool.svg"/>
                        <span><?php _e('Vanjski bazen', 'molonew'); ?></span>
                    </li>
                <?php endif; ?>
                
                
                
                <?php if( get_sub_field('vrt') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_garden.svg"/>
                        <span><?php _e('Vrt / okućnica', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('tenis') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_tenis.svg"/>
                        <span><?php _e('Teniski teren', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                
                <?php if( get_sub_field('masaza') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_hydromassage.svg"/>
                        <span><?php _e('Hidro masaža', 'molonew'); ?></span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('teretana') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_fitness.svg"/>
                        <span><?php _e('Fitness teretana', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('jacuzzy') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_jacuzy.svg"/>
                        <span><?php _e('Jacuzzi', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                
                 <?php if( get_sub_field('djeca') ): ?>
                <li>
                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_kids.svg"/>
                    <span><?php _e('Prilagođeno djeci', 'molonew'); ?></span>
                </li>
                <?php endif; ?>
                <?php if( get_sub_field('mladi') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_young.svg"/>
                        <span><?php _e('Za mlade grupe', 'molonew'); ?></span>
                    </li>
                <?php endif; ?>
                <?php if( get_sub_field('pet') ): ?>
                    <li>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_pets.svg"/>
                        <span><?php _e('Pet friendly', 'molonew'); ?> </span>
                    </li>
                <?php endif; ?>
                <?php endwhile; endif; ?>
                 
            </ul>
        <?php endwhile; endif; ?>    
    <div class="my-4 text-center">
        <button class="button amenities modal-toggle"><?php _e('Prikaži sve', 'molonew'); ?></button>
    </div>

<div class="modal--or app-meta">
    <div class="modal-overlay modal-toggle"></div>
    <div class="modal-wrapper modal-transition">
        <div class="modal-header">
            <h2><?php _e('Sadržaj', 'molonew'); ?></h2>
        </div>
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal_list">
            <?php if( have_rows('osnova_oprema') ): while ( have_rows('osnova_oprema') ) : the_row(); ?>
            <?php if( get_sub_field('klima_uredaj') ): ?>
        <div class="sep-title h5"><?php _e('Osnovno', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('privatni_parking') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Privatni parking', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('besplatan_wifi') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Besplatan wifi', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('klima_uredaj') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Klima', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('centralno') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Grijanje', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('video_intercom') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Video Intercom', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('protuprovalna_vrata') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Protuprovalna vrata', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('sef') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Sef', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('prva_pomoc') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Prva pomoć', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('protupozarni_aparat') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Protupožarni aparat', 'molonew'); ?></li>
                <?php endif; ?> 
            </ul>
        <?php endwhile; endif; ?> 
        
        <?php if( have_rows('kuhinja') ): while ( have_rows('kuhinja') ) : the_row(); ?>
        <?php if( get_sub_field('hladnjak') ): ?>
        <div class="sep-title h5"><?php _e('Kuhinja', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('vrhunski_aparati') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vrhunski aparati', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('visokokvalitetna_kuhinja') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Visokokvalitetna kuhinja', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('blagavaonica') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Blagavaonica', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('stol_u_blagavaonici') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Stol u blagavaonici', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('potpuno_opremljena_kuhinja') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Potpuno opremljena kuhinja', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('posude_i_srebrnina') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Posude i srebrnina', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('hladnjak') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Hladnjak', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('zamrzivac') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Zamrzivač', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('ledomat') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ledomat', 'molonew'); ?></li>
                <?php endif; ?> 
                
                <?php if( get_sub_field('hladnjak_za_vina') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Hladnjak za vina', 'molonew'); ?></li>
                <?php endif; ?> 
                
                <?php if( get_sub_field('pecnica') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Pećnica', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('mikrovalna') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Mikrovalna', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('toster') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Toster', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('kuhalo') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Kuhalo', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('espresso_aparat') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Aparat za espresso', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('lavazza') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Lavazza aparat za kavu', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('osnovni_pribor') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Osnovni pribor', 'molonew'); ?></li>
                <?php endif; ?>
                
                <?php if( get_sub_field('barske_stolice') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Barske stolice', 'molonew'); ?></li>
                <?php endif; ?>
            
                
                
            </ul>
        <?php endwhile; endif; ?>
        
        <?php if( have_rows('dnevna_soba') ): while ( have_rows('dnevna_soba') ) : the_row(); ?>
            <?php if( get_sub_field('veliki_dnevni_boravak') ): ?>
                <div class="sep-title h5"><?php _e('Dnevna soba', 'molonew'); ?></div>
            <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('veliki_dnevni_boravak') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Veliki dnevni boravak', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('sofa') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Sofa', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('naslonjac') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Naslonjač', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('satelitska_tv') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Satelitska TV', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('smart_tv') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Smart TV', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('igraca_konzola') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Igrača konzolaa', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('radni_stol') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Radni stol', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('ured') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ured', 'molonew'); ?></li>
                <?php endif; ?>  
            </ul>
        <?php endwhile; endif; ?> 
        
        <?php if( have_rows('spavaca_soba') ): while ( have_rows('spavaca_soba') ) : the_row(); ?>
        <?php if( get_sub_field('jastuci') ): ?>
        <div class="sep-title h5"><?php _e('Spavaća soba', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('tv_u_spavacoj_sobi') ): ?>
                <li><i class="fa fa-check"></i><?php _e('TV u spavaćoj sobi', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('posteljina') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Posteljina', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('jastuci') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Jastuci', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('ormar') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ormar', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('vjesalice') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vješalice', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('ogledalo') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ogledalo', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('krevetic_na_zahtjev') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Krevetić na zahtjev', 'molonew'); ?></li>
                <?php endif; ?>  
            </ul>
        <?php endwhile; endif; ?> 
        
        <?php if( have_rows('kupaonica') ): while ( have_rows('kupaonica') ) : the_row(); ?>
        <?php if( get_sub_field('tus') ): ?>
        <div class="sep-title h5"><?php _e('Kupaonica', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('rucnici') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ručnici', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('osnovni_dodaci') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Osnovni dodaci', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('tus') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Tuš', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('susilo_za_kosu') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Sušilo za kosu', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('perilica_rublja') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Perilica rublja', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('susilica_za_rublje') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Sušilica za rublje', 'molonew'); ?></li>
                <?php endif; ?>  
            </ul>
        <?php endwhile; endif; ?>
        
        <?php if( have_rows('bazen_&_wellness') ): while ( have_rows('bazen_&_wellness') ) : the_row(); ?>
        <?php if( get_sub_field('privatni_bazen') || get_sub_field('vanjski_bazen') ): ?>
        <div class="sep-title h5"><?php _e('Bazen & Wellness', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('privatni_bazen') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Privatni bazen', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('unutarnji_bazen') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Unutarnji bazen', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('grijani_bazen') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Grijani bazen', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('vanjski_bazen') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vanjski bazen', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('jacuzzi') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Jacuzzi', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('sauna') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Sauna', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('stol_za_masazu') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Stol za masažu', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('lezaljke') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Ležaljke', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('vanjski_tus') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vanjski tuš', 'molonew'); ?></li>
                <?php endif; ?>  
            </ul>
        <?php endwhile; endif; ?>
        
        <?php if( have_rows('vrt_i_terasa') ): while ( have_rows('vrt_i_terasa') ) : the_row(); ?>
        <?php if( get_sub_field('vrt') ): ?>
        <div class="sep-title h5"><?php _e('Vrt i terasa', 'molonew'); ?></div>
                <?php endif; ?>  
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('vrt') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vrt', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('vanjski_rostilj') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vanjski roštilj', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('vanjska_blagavaonica') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vanjska blagavaonica', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('suncobran') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Suncobran', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('terasa') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Terasa', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('balkon') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Balkon', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('pogled_na_more') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Pogled na more', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('pogled_na_planine') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Pogled na planine', 'molonew'); ?></li>
                <?php endif; ?>  
                
                 <?php if( get_sub_field('vanjski_tus') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Vanjski tuš', 'molonew'); ?></li>
                <?php endif; ?>  
            </ul>
        <?php endwhile; endif; ?>
        
        <?php if( have_rows('aktivnosti') ): while ( have_rows('aktivnosti') ) : the_row(); ?>
        <?php if( get_sub_field('soba_za_igranje') ): ?>
        <div class="sep-title h5"><?php _e('Aktivnosti', 'molonew'); ?></div>
                <?php endif; ?>
            <ul class="amenities_content single-app simple-list grid-flex">
                <?php if( get_sub_field('soba_za_igranje') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Soba za igranje', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('biljar') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Biljar', 'molonew'); ?></li>
                <?php endif; ?>  
                
                <?php if( get_sub_field('stolni_nogomet') ): ?>
                <li><i class="fa fa-check"></i><?php _e('Stolni nogomet', 'molonew'); ?></li>
                <?php endif; ?>  
                
            </ul>
        <?php endwhile; endif; ?>
        
        
        </div>
                <button class="button-primary search-filter-submit modal-toggle"><?php _e('Zatvori', 'molonew'); ?></button>
            </div>
        </div>
    </div>
</div>
        </div>
        
    </div>
    
    
    
<script>
jQuery(function($){
  $('.modal-toggle').on('click', function(e) {
    e.preventDefault();
    $('.modal--or').toggleClass('is-visible');
    $('.modal-overlay').toggleClass('is-visible');
    });
    });
</script>    
