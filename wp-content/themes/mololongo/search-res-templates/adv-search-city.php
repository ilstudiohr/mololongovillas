                <div class="modal--or">

                  <div class="modal-overlay modal-toggle"></div>

                  <div class="modal-wrapper modal-transition">

                    <div class="modal-header">

                      <!--                       <button class="modal-close modal-toggle"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="aboutUs"></button> -->

                    <h2 class="h4"><?php _e('Napredno pretraživanje', 'molonew'); ?></h2>
                      <button style="display: none;" class="modal-close--mobile modal-close modal-toggle"><img src="<?php bloginfo('template_directory');?>/images/closemolo.svg" alt="aboutUs"></button>
                    </div>



                    <div class="modal-body">

                      <div class="modal-content">

                        <div class='searchandfilter searchandfilter--or'>



                          <ul class="block detalji_smjestaja accommodation-details--or price-range--or"> 

                            <li>

                                <h3 class="h5"><?php _e('Cijena po danu', 'molonew'); ?></h3> 



                               <div class="values">

                                   <span id="range1">

                                       0

                                   </span> €

                                   <span> &dash; </span>

                                   <span id="range2">

                                       500

                                   </span> €

                               </div>

                               <div class="slider-track"></div>

                               <input name="price_min" value="<?php echo $_GET['price_min'] ?? 0 ?>" type="range" min="0" max="5000" id="slider-1" oninput="slideOne()">

                               <input name="price_max" value="<?php echo $_GET['price_max'] ?? 5000 ?>" type="range" min="0" max="5000" id="slider-2" oninput="slideTwo()">   

                            </li>               

                          </ul>



                          <ul class="block detalji_smjestaja accommodation-details--or">       



                            <h3 class="h5"><?php _e('Detalji smještaja', 'molonew'); ?></h3>

<!--                             <?php if (is_page_template( 'page-booking-search.php' )):  ?>
                                <li class="adv-locations--or">

                                  <p class="adv-locations--or__label"><?php _e('Lokacija', 'molonew'); ?>:</p>

                                  <p class="mphb_sc_search-submit-button-wrapper">
                                    <input id="advSearchLocation" type="search" name="adv_search_location" placeholder="<?php _e('Lokacija', 'molonew'); ?>"/>
                                  </p>
                              
                                </li>               

                            <?php else: ?>
                                <li class="adv-locations--or">

                                  <p class="adv-locations--or__label"><?php _e('Lokacija', 'molonew'); ?>:</p>
                                  <div class="adv-locations--or__wrapper__select">
                                    <select name="cities" id="cities" class="adv-locations--or__select">
                                      <option value=""><?php _e('Izaberi lokaciju', 'molonew'); ?></option>

                                      <option value="istra" data-lat="45.1779448" data-lng="14.0073837" data-zoom="9.6" <?php if(get_queried_object()->slug == 'istra' || get_queried_object()->slug == 'istria' || get_queried_object()->slug == 'istrien' || get_queried_object()->slug == 'istria-it' ) echo 'selected'; ?> >Istra </option>

                                      <option value="kvarner" data-lat="44.9000143" data-lng="14.5912452" data-zoom="9.8" <?php if(get_queried_object()->slug == 'kvarner' || get_queried_object()->slug == 'kvarner-en' || get_queried_object()->slug == 'kvarner-de' || get_queried_object()->slug == 'quarnero' ) echo 'selected'; ?>>Kvarner</option>

                                      <option value="dalmacija" data-lat="43.1694871" data-lng="16.5524613" data-zoom="9" <?php if(get_queried_object()->slug == 'dalmacija' || get_queried_object()->slug == 'dalmatia' || get_queried_object()->slug == 'dalmatien' || get_queried_object()->slug == 'dalmazia' ) echo 'selected'; ?>>Dalmacija</option>
                                    </select>
                                  </div>                              
                                </li>               
                            <?php endif ?> -->

                            <div style="height:0;visibility: hidden;">
                              <div class="adv-locations--or__wrapper__select">
                                <select name="cities" id="cities" class="adv-locations--or__select">
                                  <option value=""><?php _e('Izaberi lokaciju', 'molonew'); ?></option>

                                  <option value="istra" data-lat="45.1779448" data-lng="14.0073837" data-zoom="9.6" <?php if(get_queried_object()->slug == 'istra' || get_queried_object()->slug == 'istria' || get_queried_object()->slug == 'istrien' || get_queried_object()->slug == 'istria-it' ) echo 'selected'; ?> ></option>

                                  <option value="kvarner" data-lat="44.9000143" data-lng="14.5912452" data-zoom="9.8" <?php if(get_queried_object()->slug == 'kvarner' || get_queried_object()->slug == 'kvarner-en' || get_queried_object()->slug == 'kvarner-de' || get_queried_object()->slug == 'quarnero' ) echo 'selected'; ?>></option>

                                  <option value="dalmacija" data-lat="43.1694871" data-lng="16.5524613" data-zoom="9" <?php if(get_queried_object()->slug == 'dalmacija' || get_queried_object()->slug == 'dalmatia' || get_queried_object()->slug == 'dalmatien' || get_queried_object()->slug == 'dalmazia' ) echo 'selected'; ?>></option>
                                </select>
                              </div>      
                            </div>

                            <!--<li class="adv-toggle--or">
                               <p class="adv-toggle--or__label"><?php _e('Instant rezervacija', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input name="instant" type="checkbox" class="checkbox" <?php if($_GET['instant']) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li> -->
                            <!--<li class="adv-toggle--or">
                               <p class="adv-toggle--or__label"><?php _e('Prilagođeno djeci', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input type="checkbox" class="checkbox" value="djeca" name="_sfm_search_amenities[]" <?php if(in_array('djeca', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li>-->

                            <!-- <li class="adv-toggle--or">
                               <p class="adv-toggle--or__label"><?php _e('Vanjski roštilj', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input type="checkbox" class="checkbox" value="rostilj" name="_sfm_search_amenities[]" <?php if(in_array('rostilj', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li> -->

                           <!-- <li class="adv-toggle--or adv-toggle--or--pb">
                               <p class="adv-toggle--or__label"><?php _e('Polica otkazivanja', 'molonew'); ?></p>
                               <div class="adv-toggle--or__wrapper">
                                 <div class="button-cover">
                                   <div class="toggle-button--or r" id="button-1">
                                     <input type="checkbox" class="checkbox" value="polica" name="_sfm_search_amenities[]" <?php if(in_array('polica', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> />
                                     <div class="knobs"></div>
                                     <div class="layer"></div>
                                   </div>
                                 </div>
                               </div>
                            </li>-->
                            <li class="sf-field-post-meta-basic_info_rooms" data-sf-field-name="_sfm_basic_info_rooms" 

                            data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                            <p><?php _e('Broj soba', 'molonew'); ?></p>

                            <label>

                              <div class="quantity">

                                <input type="number" name="rooms" min="1" step="1" max="10" value="<?php echo $_GET['rooms'] ?? '1' ?>">

                              </div>

                            </label>

                          </li>

                            <li class="sf-field-post-meta-basic_info_beds" data-sf-field-name="_sfm_basic_info_beds" 

                            data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                            <p><?php _e('Broj kreveta', 'molonew'); ?></p>

                            <label>

                              <div class="quantity">

                                <input type="number" name="beds" min="1" step="1" max="10" value="<?php echo $_GET['beds'] ?? '1' ?>">

                              </div>

                            </label>

                          </li>



                          <li class="sf-field-post-meta-basic_info_bathrooms" data-sf-field-name="_sfm_basic_info_bathrooms" 

                          data-sf-field-type="post_meta" data-sf-field-input-type="select" data-sf-meta-type="choice">

                          <p><?php _e('Broj kupaonica', 'molonew'); ?></p>

                          <label>

                            <div class="quantity">

                              <input type="number" name="bathrooms" min="1" step="0.5" max="10" value="<?php echo $_GET['bathrooms'] ?? '1' ?>">

                            </div>

                          </label>

                        </li>

                      </ul>



                                            <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_amenities" data-sf-field-name="_sfm_search_amenities" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Sadržaji', 'molonew'); ?></h3>

                        <ul data-operator="and" class="">

                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input class="sf-input-checkbox" type="checkbox" value="jacuzzy" <?php if(in_array('jacuzzy', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c51">

                              <label for="sf-input-ef49de1d6e039b437d811b93f6957c51"><?php _e('Jacuzzi', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="vrt" <?php if(in_array('vrt', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-74b9f260ddb7515451bfdc483e3ac407">

                              <label for="sf-input-74b9f260ddb7515451bfdc483e3ac407"><?php _e('Vrt', 'molonew'); ?></label>

                            </div>

                          </li>
                          
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="rostilj" <?php if(in_array('rostilj', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-74b9f260ddb7515451bfdc483e3ac4072">

                              <label for="sf-input-74b9f260ddb7515451bfdc483e3ac4072"><?php _e('Vanjski roštilj', 'molonew'); ?></label>

                            </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="kuhinja" <?php if(in_array('kuhinja', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-74b9f260ddb7515451bfdc483e3ac4073">

                              <label for="sf-input-74b9f260ddb7515451bfdc483e3ac4073"><?php _e('Kuhinja', 'molonew'); ?></label>

                            </div>

                          </li>
                          



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="teretana" <?php if(in_array('teretana', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-b75650763000fc06f6acb4013a2adeb4">

                              <label for="sf-input-b75650763000fc06f6acb4013a2adeb4"><?php _e('Fitness teretana', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="parking" <?php if(in_array('parking', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-7597014bf01cf43045ecca07c499af97">

                              <label for="sf-input-7597014bf01cf43045ecca07c499af97"><?php _e('Privatni parking', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="bazen" <?php if(in_array('bazen', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-dc3d04965c670bd0466db24e543bfec4">

                              <label for="sf-input-dc3d04965c670bd0466db24e543bfec4"><?php _e('Bazen', 'molonew'); ?></label>

                            </div>

                          </li>



                          


                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="sauna" <?php if(in_array('sauna', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-5ad24cd8a88d970dc0ad2dec46f410d9">

                              <label for="sf-input-5ad24cd8a88d970dc0ad2dec46f410d9"><?php _e('Sauna', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="wifi" <?php if(in_array('wifi', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-51d89822c7bd1b3c7fa7563676b6cf3d">

                              <label for="sf-input-51d89822c7bd1b3c7fa7563676b6cf3d"><?php _e('Wifi', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="ograda" <?php if(in_array('ograda', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-706c73ec8340242c8844f0a5893eb39a">

                              <label for="sf-input-706c73ec8340242c8844f0a5893eb39a"><?php _e('Potpuno ograđeno', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="klima_grijanje" <?php if(in_array('klima_grijanje', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_5e285659ad054">

                              <label for="acf-field_5e2855a9a4fb4-field_5e285659ad054"><?php _e('Grijanje', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="sattv" <?php if(in_array('sattv', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_5e285657ad053">

                              <label for="acf-field_5e2855a9a4fb4-field_5e285657ad053"><?php _e('Kabelska TV', 'molonew'); ?></label>

                            </div>

                          </li>



                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="tenis" <?php if(in_array('tenis', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_602e998a2a258">

                              <label for="acf-field_5e2855a9a4fb4-field_602e998a2a258"><?php _e('Teniski teren', 'molonew'); ?></label>

                            </div>

                          </li>

                          
                        </ul>

                      </li>

                    </ul>
                    
                    <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_amenities" data-sf-field-name="_sfm_search_amenities" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Značajke', 'molonew'); ?></h3>
                        
                        <ul data-operator="and" class="">
                            
                            <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="polica" <?php if(in_array('polica', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-ef49de1d6e039b437d811b93f6957c54">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c54"><?php _e('Besplatno otkazivanje', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="self" <?php if(in_array('self', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_602e998a2a2582">

                              <label for="acf-field_5e2855a9a4fb4-field_602e998a2a2582"><?php _e('Self check-in', 'molonew'); ?></label>

                            </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="djeca" <?php if(in_array('djeca', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="acf-field_5e2855a9a4fb4-field_602e998a2a2582">

                              <label for="acf-field_5e2855a9a4fb4-field_602e998a2a2582"><?php _e('Prilagođeno djeci', 'molonew'); ?></label>

                            </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                            <div class="checkbox--or">

                              <input  class="sf-input-checkbox" type="checkbox" value="pet" <?php if(in_array('pet', (array)$_GET['_sfm_search_amenities'])) echo 'checked'; ?> name="_sfm_search_amenities[]" id="sf-input-4922c9d1395f7191f2e7f935350a656d">

                              <label for="sf-input-4922c9d1395f7191f2e7f935350a656d"><?php _e('Pet friendly', 'molonew'); ?></label>

                            </div>

                          </li>


                            
                        </ul>
                        
                        </li>

                    </ul>
                    
                    
                    <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_tema" data-sf-field-name="_sfm_search_tema" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Tema', 'molonew'); ?></h3>
                        
                        <ul data-operator="and" class="">
                            
                            <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="rustikalna_vila" <?php if(in_array('rustikalna_vila', (array)$_GET['_sfm_search_tema'])) echo 'checked'; ?> name="_sfm_search_tema[]" id="sf-input-ef49de1d6e039b437d811b93f6957c543">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c543"><?php _e('Rustikalna vila', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="suvremena_vila" <?php if(in_array('suvremena_vila', (array)$_GET['_sfm_search_tema'])) echo 'checked'; ?> name="_sfm_search_tema[]" id="sf-input-ef49de1d6e039b437d811b93f6957c544">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c544"><?php _e('Suvremena vila', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="luksuzna_vila" <?php if(in_array('luksuzna_vila', (array)$_GET['_sfm_search_tema'])) echo 'checked'; ?> name="_sfm_search_tema[]" id="sf-input-ef49de1d6e039b437d811b93f6957c545">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c545"><?php _e('Luksuzna vila', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="last_minuteposebna_ponuda" <?php if(in_array('last_minuteposebna_ponuda', (array)$_GET['_sfm_search_tema'])) echo 'checked'; ?> name="_sfm_search_tema[]" id="sf-input-ef49de1d6e039b437d811b93f6957c546">
    
                                  <label for="sf-input-ef49de1d6e039b437d811b93f6957c546"><?php _e('Last minute/Posebna ponuda', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          

                            
                        </ul>
                        
                        </li>

                    </ul>
                    
                    <ul class="block checkboxes checkboxes--or">

                        <li class="sf-field-post-meta-search_lokacija" data-sf-field-name="_sfm_search_lokacija" 

                        data-sf-field-type="post_meta" data-sf-field-input-type="checkbox" data-sf-meta-type="choice">

                        <h3 class="h5"><?php _e('Lokacija', 'molonew'); ?></h3>
                        
                        <ul data-operator="and" class="">
                            
                            <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="na_obali" <?php if(in_array('na_obali', (array)$_GET['_sfm_search_lokacija'])) echo 'checked'; ?> name="_sfm_search_lokacija[]" id="sf-input-e2f49de1d6e039b437d811b93f6957c543">
    
                                  <label for="sf-input-e2f49de1d6e039b437d811b93f6957c543"><?php _e('Na obali', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="unutrasnjost" <?php if(in_array('unutrasnjost', (array)$_GET['_sfm_search_lokacija'])) echo 'checked'; ?> name="_sfm_search_lokacija[]" id="sf-input-e2f49de1d6e039b437d811b93f6957c544">
    
                                  <label for="sf-input-e2f49de1d6e039b437d811b93f6957c544"><?php _e('Unutrašnjost', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="grad" <?php if(in_array('grad', (array)$_GET['_sfm_search_lokacija'])) echo 'checked'; ?> name="_sfm_search_lokacija[]" id="sf-input-e2f49de1d6e039b437d811b93f6957c545">
    
                                  <label for="sf-input-e2f49de1d6e039b437d811b93f6957c545"><?php _e('Grad', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          <li class="sf-level-0 " data-sf-count="-1" data-sf-depth="0">

                                <div class="checkbox--or">
    
                                  <input class="sf-input-checkbox" type="checkbox" value="selo" <?php if(in_array('selo', (array)$_GET['_sfm_search_lokacija'])) echo 'checked'; ?> name="_sfm_search_lokacija[]" id="sf-input-e2f49de1d6e039b437d811b93f6957c546">
    
                                  <label for="sf-input-e2f49de1d6e039b437d811b93f6957c546"><?php _e('Selo', 'molonew'); ?></label>
    
                                </div>

                          </li>
                          
                          

                            
                        </ul>
                        
                        </li>

                    </ul>



                    <ul class="block buttons buttons--or">

                    <!--                       <li class="sf-field-reset" data-sf-field-name="reset" data-sf-field-type="reset" data-sf-field-input-type="button">

                        <input type="submit" class="search-filter-reset" name="_sf_reset" value="Poništi" data-search-form-id="828238" data-sf-submit-form="never">

                    </li> -->

                    <li class="buttons--or__count"><div class="resource_count"></div></li>



                      <li class="sf-field-submit sf-field-submit--or--homepage" data-sf-field-name="submit" data-sf-field-type="submit" data-sf-field-input-type="">

                        <!-- <input type="submit" class="search-filter-submit" name="_sf_submit" value="Pretraži"> -->

                        <button class="search-filter-submit modal-toggle"><?php _e('Zatvori', 'molonew'); ?></button>
                        <input type="submit" class="search-filter-submit modal-toggle--off search-filter-submit--or" name="_sf_submit" value="<?php _e('Pretraži', 'molonew'); ?>">

                      </li>

                    </ul>

                  </div>

                </div>

              </div>

            </div>

          </div>