<?php get_header(); ?>	
<section class="app-top pb-0">
    <div class="container-fluid">
        <div class="row mobile-order small-gutters">
            <div class="col-md-6 mobile-one pr5 pl-0">
                <div class="app_feat-img">
                    <?php 
                    $images = get_field('gallery');
                        if($images): ?>
                        <div class="gallery">   
                            <?php $i=0; foreach( $images as $image ) : ?>
                              <a href="<?php echo $image['url']; ?>" rel="lightbox" data-lightbox="gallery">
                                <?php if( $i==0 ) : ?>
                                    <?php echo get_the_post_thumbnail( $post->ID, 'full' ); ?>
                                <?php endif; ?>
                              </a>
                            <?php $i++; endforeach; ?>
                        </div>
                    <?php endif; ?>
                    
                    
                    
                </div>
            </div>
            <div class="col-md-6 mobile-hidden pl5 pr-0">
                <div class="app-gallery">
                    <?php $images = get_field('gallery'); if ($images) { $counter = 1; ?>
                        <?php foreach( $images as $image ) { ?>	
                        	<div class="gallery-side">
                                <a href="<?php echo $image['url']; ?>" rel="lightbox" data-lightbox="gallery">
                                 <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
                                </a>
                            </div>
                        <?php $counter++; if ($counter == 5) { break; }} ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="pt-0">
    <div class="container-sm">
        <div class="row">
            <div class="col-md-12">
                <div class="info-box mt-n5">
                    <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                    <div class="info-box_single">
                        <?php if( get_sub_field('gosti') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_guests.svg"/>
                            <span><?php _e('Broj gostiju:', 'molonew'); ?> <?php the_sub_field('gosti'); ?> <?php if( get_sub_field('gosti_max') ): ?>+ <?php the_sub_field('gosti_max'); ?><?php endif; ?></span>
                        <?php endif; ?>
                    </div>

                    <div class="info-box_single">
                        <?php if( get_sub_field('beds') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg" alt="beds">
                            <span><?php _e('Broj kreveta:', 'molonew'); ?> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?></span>
                        <?php endif; ?> 
                    </div>
                    <div class="info-box_single">
                        <?php if( get_sub_field('bathrooms') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bathroom.svg" alt="<?php _e('Kupaonica', 'molonew'); ?>">
                            <span><?php _e('Broj kupaonica:', 'molonew'); ?> <?php the_sub_field('bathrooms'); ?></span>
                        <?php endif; ?>
                    </div>
                    <div class="info-box_single">
                        <?php if( get_sub_field('povrsina_ville') ): ?>
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_villa_size.svg"/>
                            <span><?php _e('Površina vile:', 'molonew'); ?> <?php the_sub_field('povrsina_ville'); ?> m<sup>2</sup></span>
                        <?php endif; ?> 
                    </div>
                    
                    <?php endwhile; endif; ?>
                    <div class="info-box_single price_box d-none">
                        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <span><?php _e('Cijena od', 'molonew'); ?></span><?php the_sub_field('price_min'); ?> <?php _e('eur/noć', 'molonew'); ?>
                        <?php endwhile; endif; ?>
                    </div>
                    
                    
                  <div class="info-box_single box_button">
 		    <?php if( have_rows('basic_info') ): ?>
                        <?php 
                             $rentlioUnitID = NULL;
                        ?>
                    <?php while ( have_rows('basic_info') ) : the_row(); 
                        $rentlioUnitID = get_sub_field('rentlioUnitID');
                        ?>
                    <?php endwhile; endif; ?>

                    <?php if($rentlioUnitID): ?>
                        <a href="#calendar"><?php _e('Rezerviraj', 'molonew'); ?></a>
                    <?php else: ?>
                        <a href="#modal_contact"><?php _e('Pošalji upit', 'molonew'); ?></a>
                    <?php endif; ?>
                        
                    </div>                </div>
            </div>
        </div>
        <div class="row app-content mt-2">
            <div class="col-md-8">
                <div class="app-info pt-4" id="overview">
                    
                    <div class="apartment-title d-flex align-center">
                        <h1 class="h2"><?php the_title(); ?></h1>
                        <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
                        <span class="ml-4 room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
                        <?php endwhile; endif; ?>
                        <a href="<?php echo do_shortcode('[supsystic-show-popup id=100]');?>"><i class="fa fa-share-alt" aria-hidden="true"></i></a>

                    </div>
                    <?php the_content();?>
                </div>
                <div class="app-info">
                    <h4 class="my-5"><?php _e('Sobe', 'molonew'); ?></h4>
                    <div class="grid-meta app-meta">
                        <ul>
                            <?php if( have_rows('room_spec') ): ?><?php $counter = 1; ?>
                            <?php while ( have_rows('room_spec') ) : the_row(); ?>
                                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_bed.svg"/>
                                    <span><?php _e('Soba', 'molonew'); ?> <?php echo $counter;?><?php $counter++; ?>: <?php if( get_sub_field('double_bed') ): ?>
                                    <?php _e('Bračni krevet', 'molonew'); ?> 
                                    <?php endif; ?>	
                                    <?php if( get_sub_field('single_bed') ): ?>
                                    <?php _e('Jednostruki krevet', 'molonew'); ?>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('room_space') ): ?>
                                    <?php _e('Kvadratura', 'molonew'); ?> <?php the_sub_field('room_space'); ?> m<sup>2</sup>
                                    <?php endif; ?>
                                    <?php if( get_sub_field('bath') ): ?>
                                    <?php _e('Kupaonica', 'molonew'); ?> 
                                    <?php endif; ?></span>
                                    
                                </li>
                            <?php endwhile; ?>
                        <?php endif; ?>
                        </ul>
                         
                    </div>
                </div>
                <?php do_action( 'mphb_render_single_room_type_before_content' ); ?>
        
                    <?php
                    /**
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderTitle				- 10
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderFeaturedImage		- 20
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderDescription		- 30
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderPrice				- 40
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderAttributes			- 50
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderCalendar			- 60
                     * @hooked \MPHB\Views\SingleRoomTypeView::renderReservationForm	- 70
                     */
                    do_action( 'mphb_render_single_room_type_content' );
                    ?>
            
                    <?php do_action( 'mphb_render_single_room_type_after_content' ); ?>
            </div>
            <div class="col-md-4">
                <div id="sidebar">
                    <div class="sidebar__inner">
                        <div class="sidebar-list pt-4">
                            <ul>
                                <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                                    <?php if( get_sub_field('check-in') ): ?>
                                        <li>
                                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_clock.svg" />
                                            <span><?php _e('Check-in:', 'molonew'); ?> <?php the_sub_field('check-in'); ?><br> <?php _e('Check-out:', 'molonew'); ?> <?php the_sub_field('check-out'); ?></span>
                                        </li>
                                    <?php endif; ?>	
                                <?php endwhile; endif; ?>
                                <?php if( have_rows('amenities') ): while ( have_rows('amenities') ) : the_row(); ?>
                                
                                    <?php
                                        $ljubimci = get_sub_field('pet');
                                        if( $ljubimci && in_array('yes', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Dopušteni ljubimci', 'molonew'); ?></span>
                                            </li>
                                        <?php elseif( $ljubimci && in_array('no', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Nisu dopušteni ljubimci', 'molonew'); ?></span>
                                            </li>
                                        <?php elseif( $ljubimci && in_array('upit', $ljubimci) ) :?> 
                                            <li>
                                            	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljubimci.svg"/>
                                                <span><?php _e('Ljubimci na upit', 'molonew'); ?></span>
                                            </li>
                                    <?php endif;?>
                                    
                                    <?php
                                        $mladi = get_sub_field('mladi');
                                        if( $mladi && in_array('yes', $mladi) ) :?> 
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_young.svg"/>
                                            <span><?php _e('Za mlade grupe', 'molonew'); ?></span>
                                        </li>
                                    <?php elseif( $mladi && in_array('no', $mladi) ) :?> 
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_young.svg"/>
                                            <span><?php _e('Nije za mlade grupe', 'molonew'); ?></span>
                                        </li>
                                    <?php endif;?>

                                    <?php if( get_sub_field('polog') ): ?>
                                        <li><button id="modal_safe" class="no-button">
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/deposit.svg"/>
                                            <span><?php _e('Sigurnosni polog', 'molonew'); ?></span>
                                            </button>
                                        </li>
                                    <?php endif; ?>	
                                    <?php if( get_sub_field('self') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-self.svg"/>
                                            <span><?php _e('Self check-in', 'molonew'); ?></span>
                                        </li>
                                    <?php endif; ?>	
                                    <?php if( get_sub_field('polica') ): ?>
                                        <li>
                                        	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/polica.svg"/>
                                            <span><?php _e('Polica otkazivanja', 'molonew'); ?></span>
                                        </li>
                                    <?php endif; ?>	
                                    <li>
                                    <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/phone.svg"/>
                                    <div class="d-flex flex-column">
                                        <span><?php _e('Trebate pomoć?', 'molonew'); ?></span>
                                    <span><a class="text-body" href="tel:+385 91 600 6646">+385 91 600 6646</a></span>
                                    </div>
                                     
                                    
                                </li>
                                <?php endwhile; endif; ?>

                            </ul>
                            <div class="sidebar-button_group">
 
                    		<?php if( have_rows('basic_info') ): ?>
                        	  <?php 
                             	    $rentlioUnitID = NULL;
                                    $isEnquiry = NULL;
                                  ?>
                       		<?php while ( have_rows('basic_info') ) : the_row(); 
                            	    $rentlioUnitID = get_sub_field('rentlioUnitID');
                                    $isEnquiry = get_sub_field('no_instant_booking');
                                ?>
                     		<?php endwhile; endif; ?>

                     		<?php if($rentlioUnitID && !$isEnquiry): ?>
            			   <a href="#calendar" class="button-primary" style="border-top-right-radius: 0;border-bottom-right-radius: 0;width: 100%;"><?php _e('Rezerviraj', 'molonew'); ?></a>
                                   <button id="modal_contact" class="button-secondary" style="border-top-left-radius: 0;border-bottom-left-radius: 0;width: 100%;"><?php _e('Pošalji upit', 'molonew'); ?></button>
                    		<?php else: ?>
                                <button id="modal_contact" class="button-secondary" style="width: 100%;"><?php _e('Pošalji upit', 'molonew'); ?></button>                    <?php endif; ?>     
                            </div>
                            

                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="p-0">
    <div class="container-fluid">
        <div class="row">
               		<?php if( have_rows('basic_info') ): ?>
                    	  <?php 
                            $rentlioUnitID = NULL;
			    $isEnquiry = NULL;
                        ?>
                        <?php while ( have_rows('basic_info') ) : the_row(); 
                    	    $rentlioUnitID = get_sub_field('rentlioUnitID');
		     	    $isEnquiry = get_sub_field('no_instant_booking');
                    	?>
                	<?php endwhile; endif; ?>

                	<?php if($rentlioUnitID): ?>
                    	    <?php $url = wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ); ?>
    		            <div class="container-sm">
                               <div class="row">
                                  <div class="col-md-12">
		                     <div class="app-info pt-md-4">
              		                <h3 class="h4 single-title text-center"  id="calendar"><?php _e('Kalendar raspoloživosti', 'molonew'); ?></h3>
            	                     </div>
	                         </div>
                               </div>
                            </div>
                            <div class="col-md-12">
                                <div class="ml-engine-calendar" isEnquiry="<?php if($isEnquiry) echo 'true' ?>" unitTypeId="<?php echo $rentlioUnitID ?>" unitImgSrc="<?php echo $url ?>" language="<?php if (ICL_LANGUAGE_CODE == 'en'): ?>en<?php elseif (ICL_LANGUAGE_CODE == 'hr'): ?>hr<?php elseif (ICL_LANGUAGE_CODE == 'de'): ?>de<?php elseif (ICL_LANGUAGE_CODE == 'it'): ?>it<?php endif; ?>"></div>
                            </div>
                         <?php endif; ?>
        </div>
    </div>
</section>
                    
<section class="p-0 d-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                    <div class="app-info">
                    <h4 class="my-5"><?php _e('Cijene', 'molonew'); ?></h4>
                    <div class="app-pricing">
                    	<div class="periods pt-0">
                    		<div class="periods_inner">
                            
                                <ul class="tabs nav nav-tabs">
                                    <li class="active"><a href="#tab1" data-toggle="tab">HRK</a></li>
                                    <li><a href="#tab2" data-toggle="tab">EUR</a></li>
                                    <li><a href="#tab3" data-toggle="tab">USD</a></li>
                                    <li><a href="#tab4" data-toggle="tab">GBP</a></li>
                                </ul>
                                
                                <div class="tab-content">
                                    
                                <div class="tab tab-pane active" id="tab1">
                                    <div class="block period_row">
                                        <span class="period"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Zeitraum<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Periodo<?php endif; ?></span>
                                        <span class="nights"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Minimalni broj noćenja<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Minimum number of nights<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Mindestanzahl von Nächten<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Numero minimo di notti<?php endif; ?></span>
                                        <span class="price"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Cijena po periodu<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Price per period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Preis pro Periode<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prezzo per periodo<?php endif; ?></span>
                                    </div>
                                    
                                    <?php if( have_rows('periodi') ): while ( have_rows('periodi') ) : the_row(); ?><div class="block period_row">
                                        <span class="period"><?php the_sub_field('pocetak'); ?> - <?php the_sub_field('kraj'); ?></span>
                                        <span class="nights"><?php the_sub_field('nocenje_min'); ?></span>
                                        <span class="price"><?php the_sub_field('cijena_hrk'); ?> Kn</span>
                                    </div><?php endwhile; endif; ?>	
                                </div>
                                    
                                <div class="tab  tab-pane" id="tab2">
                                    <div class="block period_row">
                                        <span class="period"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Zeitraum<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Periodo<?php endif; ?></span>
                                        <span class="nights"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Minimalni broj noćenja<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Minimum number of nights<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Mindestanzahl von Nächten<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Numero minimo di notti<?php endif; ?></span>
                                        <span class="price"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Cijena po periodu<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Price per period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Preis pro Periode<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prezzo per periodo<?php endif; ?></span>
                                    </div>
                                    
                                    <?php if( have_rows('periodi') ): while ( have_rows('periodi') ) : the_row(); ?><div class="block period_row">
                                        <span class="period"><?php the_sub_field('pocetak'); ?> - <?php the_sub_field('kraj'); ?></span>
                                        <span class="nights"><?php the_sub_field('nocenje_min'); ?></span>
                                        <span class="price"><?php the_sub_field('cijena_eur'); ?> €</span>
                                    </div><?php endwhile; endif; ?>	
                                </div>
                                    
                                <div class="tab tab-pane" id="tab3">
                                    <div class="block period_row">
                                        <span class="period"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Zeitraum<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Periodo<?php endif; ?></span>
                                        <span class="nights"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Minimalni broj noćenja<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Minimum number of nights<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Mindestanzahl von Nächten<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Numero minimo di notti<?php endif; ?></span>
                                        <span class="price"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Cijena po periodu<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Price per period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Preis pro Periode<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prezzo per periodo<?php endif; ?></span>
                                    </div>
                                    
                                    <?php if( have_rows('periodi') ): while ( have_rows('periodi') ) : the_row(); ?><div class="block period_row">
                                        <span class="period"><?php the_sub_field('pocetak'); ?> - <?php the_sub_field('kraj'); ?></span>
                                        <span class="nights"><?php the_sub_field('nocenje_min'); ?></span>
                                        <span class="price"><?php the_sub_field('cijena_usd'); ?> $</span>
                                    </div><?php endwhile; endif; ?>	
                                </div>
                                    
                                <div class="tab tab-pane" id="tab4">
                                    <div class="block period_row">
                                        <span class="period"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Zeitraum<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Periodo<?php endif; ?></span>
                                        <span class="nights"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Minimalni broj noćenja<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Minimum number of nights<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Mindestanzahl von Nächten<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Numero minimo di notti<?php endif; ?></span>
                                        <span class="price"><?php if(ICL_LANGUAGE_CODE=='hr'): ?>Cijena po periodu<?php elseif(ICL_LANGUAGE_CODE=='en'): ?>Price per period<?php elseif(ICL_LANGUAGE_CODE=='de'): ?>Preis pro Periode<?php elseif(ICL_LANGUAGE_CODE=='it'): ?>Prezzo per periodo<?php endif; ?></span>
                                    </div>
                                    
                                    <?php if( have_rows('periodi') ): while ( have_rows('periodi') ) : the_row(); ?><div class="block period_row">
                                        <span class="period"><?php the_sub_field('pocetak'); ?> - <?php the_sub_field('kraj'); ?></span>
                                        <span class="nights"><?php the_sub_field('nocenje_min'); ?></span>
                                        <span class="price"><?php the_sub_field('cijena_gbp'); ?> &pound;</span>
                                    </div><?php endwhile; endif; ?>	
                                </div>
                                
                                </div>
                                
                    			<?php if(ICL_LANGUAGE_CODE=='hr'): ?><small>Cijena uključuje: PDV, boravišnu pristojbu i sve navedeno na oglasu vile.<br />
                                Osiguranje od štete: obavezan je povratni polog u iznosu 300,00 € ili nepovratno osiguranje od štete u iznosu 30€.</small>
                                <?php elseif(ICL_LANGUAGE_CODE=='en'): ?><small>The price includes: VAT, tourist tax and everything listed on the villa's ad.<br />
                    Damage insurance: mandatory return deposit in the amount of € 300.00 or non-refundable damage insurance in the amount of € 30.</small>
                                <?php elseif(ICL_LANGUAGE_CODE=='de'): ?><small>Der Preis beinhaltet: Mehrwertsteuer, Kurtaxe und alles, was in der Anzeige der Villa aufgeführt ist.<br />
                    Schadensversicherung: obligatorische Rückzahlung in Höhe von 300,00 € oder nicht erstattungsfähige Schadensversicherung in Höhe von 30 €.</small>
                                <?php elseif(ICL_LANGUAGE_CODE=='it'): ?><small>Il prezzo include: IVA, tassa di soggiorno e tutto quanto indicato nell'annuncio della villa.<br />
                    Assicurazione danni: cauzione obbligatoria di restituzione per un importo di € 300,00 o assicurazione danni non rimborsabile per un importo di € 30.</small>
                                <?php endif; ?>	
                    		
                    		</div>
                    	</div>
                    </div>
                    </div>
            </div>
        </div>
    </div>
</section>
                    
<section class="p-0">
    <div class="container-md">
        <div class="row">
            <div class="col-md-12">
                <div class="app-info">
                    <h4 class="text-left my-5"><?php _e('Opis lokacije', 'molonew'); ?></h4>
                    <div class="app-location">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="app-info">
                                    <div class="add-info ">
                                        <div class="app-desc addReadMore showlesscontent">
                                            <?php if( get_field('lokacija_info') ): ?>
                                                <?php the_field('lokacija_info'); ?>
                                            <?php endif; ?> 
                                        </div>
                                    </div>
                                </div>
                        </div>
                            <div class="col-md-4">
                                <div class="app-info">
                                    <div class="sidebar-list distance-meta">
                                        <div class="app-meta block-meta distance-meta">
                                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                                            <ul>
                                                <?php if( get_sub_field('aerodrom') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_airport.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Aerodrom', 'molonew'); ?></span> <span> <?php the_sub_field('aerodrom'); ?></span></span>
                                                </li>
                                                <?php endif; ?>  
                                                
                                                <?php if( get_sub_field('luka') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/luka.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Luka', 'molonew'); ?></span> <span> <?php the_sub_field('luka'); ?></span></span>
                                                </li>
                                                <?php endif; ?> 
                                                <?php if( get_sub_field('beach') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/icon_beach.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Plaža', 'molonew'); ?></span> <span> <?php the_sub_field('beach'); ?></span></span>
                                                </li>
                                                <?php endif; ?> 
                                                <?php if( get_sub_field('restaurant') ): ?>
                                                <li>
                                                	<img  style="width:26px" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico-pribor.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Restoran', 'molonew'); ?></span> <span> <?php the_sub_field('restaurant'); ?></span></span>
                                                </li>
                                                <?php endif; ?> 
                                                <?php if( get_sub_field('shop') ): ?>
                                                <li>
                                                	<img style="width:26px" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/trgovina.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Trgovina', 'molonew'); ?></span> <span><?php the_sub_field('shop'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('ljekarna') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ljekarna.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Ljekarna', 'molonew'); ?></span> <span><?php the_sub_field('ljekarna'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('supermarket') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/supermarket.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Supermarket', 'molonew'); ?></span> <span><?php the_sub_field('supermarket'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('bolnica') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bolnica.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Bolnica', 'molonew'); ?></span> <span><?php the_sub_field('bolnica'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('prvapomoc') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/prvapomoc.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Prva pomoć', 'molonew'); ?></span> <span><?php the_sub_field('prvapomoc'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('kasino') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/casino.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Kasino', 'molonew'); ?></span> <span><?php the_sub_field('kasino'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('nocniklub') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/nocniklub.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Noćni klub', 'molonew'); ?></span> <span><?php the_sub_field('nocniklub'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                                <?php if( get_sub_field('pub') ): ?>
                                                <li>
                                                	<img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pub.svg"/>
                                                	<span class="d-flex justify-content-between w-100"><span><?php _e('Pub', 'molonew'); ?></span> <span><?php the_sub_field('pub'); ?></span></span>
                                                </li>
                                                <?php endif; ?>
                                            </ul>

                        
                                            <?php endwhile; endif; ?>
                    </div>
                    </div>
                            </div>
                        </div>
                        
                    </div>
            </div>
                        
                    </div>
                </div>
                </div>
            </div>
</section>

<section>
    <div class="app-info" id="map">
        <div class="map-frame mb-4">
            <div id="dd_map"></div>
        </div>
    </div>
</section>
<section class=" py-0">
    <div class="container">
        <div class="row">
            <div class="col-md-12 mb-4">
                <h3 class="single-title h4"><?php _e('Slične smještajne jedinice', 'molonew'); ?></h3>
            </div>
            <div class="swiper swiper-featured pt-5 mt-n5 pb-4">
                <div class="swiper-wrapper">
                <?php $terms = get_the_terms( $post->ID , 'mphb_ra_lokacija', 'string'); $term_ids = wp_list_pluck($terms,'term_id');
                $second_query = new WP_Query( array('post_type' => 'mphb_room_type','tax_query' => array(
                array('taxonomy' => 'mphb_ra_lokacija','field' => 'id','terms' => $term_ids,'operator'=> 'IN')),
                'posts_per_page' => 4,'ignore_sticky_posts' => 1,'orderby' => 'rand','post__not_in'=>array($post->ID)));
                if($second_query->have_posts()) { while ($second_query->have_posts() ) : $second_query->the_post(); ?>	
                    <div class="swiper-slide">
                        <div class="col-md-12">
                            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>
                            <div class="app-box">
            				    <div class="app-image">
            				        <div class="swiper swiper-id" id="post-<?php the_ID(); ?>">
            				            <div class="swiper-wrapper">
            				                <?php $images = get_field('gallery'); if ($images) { $counter = 1; ?>
                                                <?php foreach( $images as $image ) { ?>	
            				                        <div class="swiper-slide">
            				                            <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="<?php echo $image['alt']; ?>" />
            				                        </div>
        				                        <?php $counter++; if ($counter == 10) { break; }} ?>
                                            <?php } ?>
                                            
            				            </div>
            				            <div class="swiper-button-prev"></div>
                                            <div class="swiper-button-next"></div>
            				        </div>
            				        <span class="app-stars app_stars<?php the_sub_field('stars'); ?>"></span>
            				    </div>
            				    <div class="app-content">
            				        <a href="<?php the_permalink() ?>" class="link-abs"></a>
            				        <div class="app-title">
            				            <h4><?php the_title(); ?></h4>
            				        </div>
            				        <div class="app-icons">
            				            <ul class="small-icons">
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin-color.svg" alt="<?php _e('Lokacija', 'molonew'); ?>"> <?php the_sub_field('city'); ?>	</li>
            				                <?php if( get_sub_field('gosti') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_guests-color.svg" alt="<?php _e('Kapacitet', 'molonew'); ?>"> <?php the_sub_field('gosti'); if( get_sub_field('gosti_max') ): ?> + <?php the_sub_field('gosti_max'); endif; ?></li>
            				                <?php endif; ?>  
            				                <?php if( get_sub_field('beds') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/ico_bed-color.svg" alt="<?php _e('Broj kreveta', 'molonew'); ?>"> <?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?> </li>
            				                <?php endif; ?>
            				                <?php if( get_sub_field('bathrooms') ): ?>
            				                <li><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/bath.svg" alt="<?php _e('Broj kupaona', 'molonew'); ?>"> <?php the_sub_field('bathrooms'); ?> </li>
            				                <?php endif; ?>
            				            </ul>
            				        </div>
            				        <div class="app-short">
            				            <p><?php echo excerpt(22); ?></p>
            				        </div>
            				        <div class="app-button">
            				            <a href="<?php the_permalink() ?>" class="button-primary"><?php _e('Rezerviraj', 'molonew'); ?></a>
            				        </div>
            				    </div>
            				    
            				</div>
            				<?php endwhile; endif; ?>
                        </div>
                    </div>
                <?php endwhile; wp_reset_query(); } ?>
              </div>
            
              <!-- If we need navigation buttons -->
              <div class="swiper-button-prev pos-tr"></div>
              <div class="swiper-button-next pos-tr"></div>

            </div>
        </div>
    </div>
    
</section>

<div class="mobile-action">
    <a href="#calendar"><?php _e('Rezerviraj', 'molonew'); ?></a>
</div>
<!-- The Modal -->
<div id="modal_content" class="modal_wrapper">

  <!-- Modal content -->
  <div class="modal_content">
    <span class="close">&times;</span>
    <h6><?php _e('Kontaktirajte nas', 'molonew'); ?></h6>
    <p><?php _e('Imate li pitanje u vezi rezervacije vile ili njezinoj dostupnosti? Kontaktirajte nas i odgovorit ćemo vam u najkraćem mogućem roku.', 'molonew'); ?></p>
    <div class="app-form-content">
        <div class="img-app">
            <img src="<?php the_post_thumbnail_url('thumbnail'); ?>" alt="Apartment image" />
        </div>
        <div class="app-content">
            <?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
            <div class="modal_info">
                <span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span>
                <h4><?php the_title(); ?></h4>
                
                <span><?php the_sub_field('address'); ?> <?php the_sub_field('city'); ?></span>
                
            </div><?php endwhile; endif; ?>
        </div>
    </div>
    
    	
    <?php echo do_shortcode('[contact-form-7 id="848740" title="Slanje upita"]'); ?>
  </div>

</div>

<div id="modal_polog" class="modal_wrapper">

  <!-- Modal content -->
  <div class="modal_content">
    <span class="close2">&times;</span>
    <h6><?php _e('Kontaktirajte nas', 'molonew'); ?></h6>
    <p><?php _e('Imate pitanje u vezi rezervacije apartmana ili njegovoj dostupnosti? Kontaktirajte nas i odgovorit ćemo vam u najkraćem mogućem roku', 'molonew'); ?></p>
    	  </div>

</div>
<?php

/**

 * @hooked \MPHB\Views\SingleRoomTypeView::renderPageWrapperEnd - 10

 */

do_action( 'mphb_render_single_room_type_wrapper_end' );

?>
<script>
var modal = document.getElementById("modal_content");
var btn = document.getElementById("modal_contact");
var span = document.getElementsByClassName("close")[0];
btn.onclick = function() {
  modal.style.display = "flex";
}
span.onclick = function() {
  modal.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>
<script>
var modal2 = document.getElementById("modal_polog");
var btn2 = document.getElementById("modal_safe");
var span2 = document.getElementsByClassName("close2")[0];
btn2.onclick = function() {
  modal2.style.display = "flex";
}
span2.onclick = function() {
  modal2.style.display = "none";
}
window.onclick = function(event) {
  if (event.target == modal2) {
    modal2.style.display = "none";
  }
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCxNs-xT0afx_k-D69SLU8dvmn5oPnKkFE"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/markerclusterer.js"></script>
<script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/sticky-sidebar.min.js"></script>

<script>
    function initMap(){
          // map options
          var options = {
            zoom:10,
            center:{<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?><?php endwhile; endif; ?>},
			mapTypeId: "roadmap",
    		// disableDefaultUI: true,
			styles: [

				{
					featureType: "all",
					elementType: "all",
					stylers: [
					  { "saturation": -100 }
					]
				},
				{
					"featureType": "landscape",
					"elementType": "all",
					"stylers": [
						{ "saturation": -100 },
						{ "lightness": 65 },
						{ "visibility": "on" }
					]
				},				
				
				{
					"featureType": "water",
					"elementType": "geometry",
					"stylers": [
						{ "hue": "#ffff00" },
						{ "lightness": -10 },
						{ "saturation": -100 }
					]
				},
				{
					"featureType": "water",
					"elementType": "labels",
					"stylers": [
						{ "lightness": -25 },
						{ "saturation": -100 }
					]
				}
			]
          }
    var map = new google.maps.Map(document.getElementById('dd_map'),options);

          var markers = [
				<?php if( have_rows('basic_info') ): while ( have_rows('basic_info') ) : the_row(); ?>	
						<?php if( get_sub_field('latitude') ): ?>{
						  coords:{lat:<?php the_sub_field('latitude'); ?>, lng:<?php the_sub_field('longitude'); ?>},
						  iconImage:'<?php echo esc_url(get_template_directory_uri()); ?>/images/marker_3.png',
						  content:
							'<p class="dd_map_title"><?php the_title(); ?></p>' +
							'<p class="dd_map_p small-icons"><span class="room_stars room_stars_left<?php the_sub_field('stars'); ?>"></span></p>' +
							
							'<p class="dd_map_p small-icons"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/pin.png" /><?php the_sub_field('address'); ?>, <?php the_sub_field('city'); ?></p>' +
							
							'<p class="dd_map_p small-icons align-items-center d-flex"><span class="dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/map_guests.png" /><?php the_sub_field('basic_capacity'); if( get_sub_field('spare_capacity') ): ?> &#43; <?php the_sub_field('spare_capacity'); endif; ?></span><span class="dd_map_half"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/map_beds.png" /><?php the_sub_field('beds'); if( get_sub_field('spare_beds') ): ?> + <?php the_sub_field('spare_beds'); endif; ?></span></p>' +
							'<p class="dd_map_p">Cijena od: <strong><?php the_sub_field('price'); ?> € <small>za 3 noći</small></strong></p>' +
							'<div style="padding:0 18px 10px 18px"><a class="button dd_map_button dd_map_button2" target="_blank" href="<?php the_sub_field('google_map'); ?>" target="_blank">Pogledaj na karti</a></div>'
						},<?php endif; ?>
				<?php endwhile; endif; ?>	
          ];
          // Loop through markers
          var gmarkers = [];
          for(var i = 0; i < markers.length; i++){
            gmarkers.push(addMarker(markers[i]));

          }

          //Add MArker function
          function addMarker(props){
            var marker = new google.maps.Marker({
              position:props.coords,
              map:map,

            });

            if(props.iconImage){
              marker.setIcon(props.iconImage);
            }

            //Check content
            if(props.content){
              var infoWindow = new google.maps.InfoWindow({
                content:props.content
              });
              marker.addListener('click',function(){
                infoWindow.open(map,marker);
              });
            }
            return marker;
          }
		  
        var markerCluster = new MarkerClusterer(map, gmarkers, 
          {
            // for default marker sizes (m1, m2, m3...)
			//imagePath: '<?php echo esc_url(get_template_directory_uri()); ?>/images/m'
			
			styles:[{
			
				url: "<?php echo esc_url(get_template_directory_uri()); ?>/images/m5.png",
					width: 90,
					height:90,
					fontFamily:"Nunito Sans",
					textSize:16,
					textColor:"#ffffff",
					//color: #00FF00,
			}]			
			
          });
		  
        }
    google.maps.event.addDomListener(window, 'load', initMap);
</script>
<script>
    jQuery(function($){
    function getLocation(){
        if(navigator.geolocation){
            navigator.geolocation.getCurrentPosition(getWeather);
        }else{
            alert("Geolocation not supported by this browser");
        }
    }
    function getWeather(position){
        let lat = '45.337620';
        let long = '14.305196';
        let API_KEY = '2398e46ce4a606d494453c5342cc0b97';
        let baseURL = `https://api.openweathermap.org/data/2.5/onecall?&lat=${lat}&lon=${long}&appid=${API_KEY}`;
        
        

        $.get(baseURL,function(res){
            let data = res.current;
            let temp = Math.floor(data.temp - 273);
            let condition = data.weather[0].description;

            $('#temp-main').html(`${temp}°`);
            $('#condition').html(condition);
        })
        
    }

    getLocation();
})
</script>
<script
      type="text/javascript"
      src="https://booking.mololongoaccommodation.com/build/engine.js"
      async
  ></script>
  
  <script>
      jQuery(function($) {

  function AddReadMore() {
    var carLmt = 120;
    var readMore = "<button class='readMore button amenities mb-5'  title='Click to Show More'> <?php _e('Pročitaj više', 'molonew'); ?> </button>";
    var readLess = "<button class='readLess button amenities mb-5' title='Click to Show Less'> <?php _e('Sakrij', 'molonew'); ?></button>";

    $(".addReadMore").each(function() {
      var allstr = $(this).text();

      if (allstr.length > carLmt && window.innerWidth <= 767) {
        var firstSet = allstr.substring(0, carLmt);
        var secdHalf = allstr.substring(carLmt, allstr.length);
        var strtoadd = firstSet + "<span class='SecSec'>" + secdHalf + "</span>" + readMore + readLess;

        $(this).html(strtoadd);
      }
    });

    $(".readMore, .readLess").on("click", function() {
      $(this).closest(".addReadMore").toggleClass("showlesscontent showmorecontent");
    });
  }

  $(window).on('resize', function() {
    $(".readMore, .readLess").off("click").remove();
    $('.addReadMore').each(function() {
      $(this).html($(this).text());
    });

    AddReadMore();
  });

  AddReadMore();

});
  </script>
<?php get_footer(''); ?>	
 