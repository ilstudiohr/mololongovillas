<?php
/*
Plugin Name: DateRange Picker
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/daterange-picker-plugin/
Description: Multipurpose Date Range Picker - WordPress Plugin
Version: 2.3.0
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgsdaterangepicker
Domain Path: /languages
*/
define('MGS_DATERANGEPICKER_VERSION', '2.3.0');
define('MGS_DATERANGEPICKER_PLUGIN_DIR', str_replace('\\','/',dirname(__FILE__)));

if ( !class_exists( 'MgsDateRangePickerInit' ) ) {

	class MgsDateRangePickerInit {

		function __construct() {
			
			add_action( 'init', array( $this, 'mgsdaterangepicker_init' ) );
			add_action( 'admin_init', array( $this, 'mgsdaterangepicker_admin_init' ) );
			add_action( 'admin_menu', array( $this, 'mgsdaterangepicker_admin_menu' ) );
			add_action( 'wp_footer', array( $this, 'mgsdaterangepicker_wp_footer' ) );
			add_action( 'wp_enqueue_scripts', array( $this, 'mgsdaterangepicker_fileenqueue' ) );
			add_action( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, 'mgsdaterangepickerplugin_action_links_add' ) );
			
		}
			
		function mgsdaterangepicker_init() {
			
			$domain = 'mgsdaterangepicker';
			load_plugin_textdomain( $domain, FALSE, basename( dirname( __FILE__ ) ) . '/languages' );
			
		}
		
		function mgsdaterangepicker_fileenqueue() {
				
			$mgsdaterangepicker_ver = '2.3.0';
			
			wp_enqueue_style('fontawesome', plugin_dir_url( __FILE__ ) . 'css/fontawesome/all.min.css');
			
			wp_enqueue_style('daterangepicker', plugin_dir_url( __FILE__ ) . 'css/daterangepicker.css');
			
			wp_enqueue_style('calendar-daterange-picker', plugin_dir_url( __FILE__ ) . 'css/calendar-daterange-picker.css', array(), $mgsdaterangepicker_ver);
			
			$mgsdaterangepickercallang = get_option( 'mgsdaterangepicker_calendar_language', '' );
			if($mgsdaterangepickercallang == 'Other')
				wp_enqueue_script('moment-with-locales', plugin_dir_url( __FILE__ ) . 'js/moment-with-locales.min.js', array('jquery'));
			else
				wp_enqueue_script('moment', plugin_dir_url( __FILE__ ) . 'js/moment.min.js', array('jquery'));
			
		}		
		
		function mgsdaterangepicker_admin_init() {

			//Register settings for required DateRange Picker scripts
			register_setting( 'mgsdaterangepicker', 'mgsdaterangepicker_calendar_language', 'trim' );
			register_setting( 'mgsdaterangepicker', 'mgsdaterangepicker_custom_scripts', 'trim' );
			
		}

		//adds menu item to wordpress admin dashboard
		function mgsdaterangepicker_admin_menu() {
			$page = add_submenu_page( 'options-general.php', esc_html__('DateRange Picker Custom Scripts', 'mgsdaterangepicker'), esc_html__('DateRange Picker Custom Scripts', 'mgsdaterangepicker'), 'manage_options', __FILE__, array( $this, 'mgsdaterangepicker_options_panel' ) );
		}

		function mgsdaterangepicker_options_panel() {
			//Load options page
			require_once(MGS_DATERANGEPICKER_PLUGIN_DIR . '/inc/mgsdaterangepicker-options.php');
		}
		
		function mgsdaterangepickerplugin_action_links_add( $links ) {
			
			$settings_link = '<a href="options-general.php?page=mgsdaterangepicker/mgsdaterangepicker.php">' . esc_html__('Settings', 'mgsdaterangepicker') . '</a>';
			array_unshift( $links, $settings_link );
			return $links;
			
		}
		
		function mgsdaterangepicker_wp_footer() {
			
			$text = get_option( 'mgsdaterangepicker_custom_scripts', '' );
			$text = convert_smilies( $text );
			$text = do_shortcode( $text );

			if ( $text != '' ) {
							
				$mgsdaterangepickerjscripts = '';
				$mgsdaterangepickerjscripts .= '(function ($) {';
				$mgsdaterangepickerjscripts .="\n";
				$mgsdaterangepickerjscripts .='"use strict";'; 
				$mgsdaterangepickerjscripts .="\n\n";
				$mgsdaterangepickerjscripts .=$text . "\n\n";
				$mgsdaterangepickerjscripts .='})(jQuery);';				
			}
			
			wp_enqueue_script('daterangepickerjs', plugin_dir_url( __FILE__ ) . 'js/daterangepicker.js', array('jquery'));
			wp_add_inline_script( 'daterangepickerjs', $mgsdaterangepickerjscripts );
			
		}
				
		
	}
	$mgsdaterangepicker = new MgsDateRangePickerInit();
}
		
?>
