<?php
/*
Plugin Name: DateRange Picker
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/daterange-picker-plugin/
Description: Multipurpose Date Range Picker - WordPress Plugin
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgsdaterangepicker
*/
?>
<div class="wrap">
	<h2><?php esc_html_e( 'DateRange Picker Custom Scripts - For Your Required Calendar', 'mgsdaterangepicker'); ?> - <strong><a href="<?php echo esc_url( "//docs.mgscoder.com/daterange-picker-documentation/" ); ?>" class="button" target="_blank"><?php esc_html_e('View Documentation', 'mgsdaterangepicker'); ?></a></strong></h2>
	<hr />
	<div id="poststuff">
		<div id="post-body" class="metabox-holder columns-2">
			<div id="post-body-content">
				<div class="postbox">
					<div class="inside">
						<form name="dofollow" action="options.php" method="post">
							
							<img src="<?php echo esc_url( plugins_url( 'images/mgsdaterangepicker-calendar-language.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'mgsdaterangepicker-calendar-language', 'mgsdaterangepicker'); ?>" width="97%" />
							<hr />
							<h3 class="footerlabel" for="mgsdaterangepicker_calendar_language"><?php esc_html_e( 'DateRange Picker Calendar Language:', 'mgsdaterangepicker'); ?></h3>
							<p>
								<input name="mgsdaterangepicker_calendar_language" id="mgsdaterangepicker_calendar_language1" class="form-control" type="radio" value="English"<?php if( esc_html( get_option( 'mgsdaterangepicker_calendar_language' ) ) == 'English' || esc_html( get_option( 'mgsdaterangepicker_calendar_language' ) ) == '' ) echo ' checked="checked"'; ?>> <?php esc_html_e( 'English', 'mgsdaterangepicker'); ?> &nbsp;
								<input name="mgsdaterangepicker_calendar_language" id="mgsdaterangepicker_calendar_language2" class="form-control" type="radio" value="Other"<?php if( esc_html( get_option( 'mgsdaterangepicker_calendar_language' ) ) == 'Other' ) echo ' checked="checked"'; ?>> <?php esc_html_e( 'Other', 'mgsdaterangepicker'); ?>
							</p>
							<h4><strong><?php esc_html_e( 'If Your required Calendar Language is not English: Put the following js code in the bellow DateRange Picker Custom Scripts settings option at very top:', 'mgsdaterangepicker'); ?></strong></h4>
							<p><code>moment.locale('fr');</code></p>
							<h3 class="hndle"><?php esc_html_e( '- where "fr" is the Language Code. You have to change "fr" with your required Language Code', 'mgsdaterangepicker'); ?></h3>
							
							<?php settings_fields( 'mgsdaterangepicker' ); ?>
							
							<h3 class="footerlabel" for="mgsdaterangepicker_custom_scripts"><?php esc_html_e( 'DateRange Picker Custom Scripts [Do not use: &lt;script&gt; tag, Just write the js code for your settings]:', 'mgsdaterangepicker'); ?></h3>
							<textarea style="width:98%;" rows="60" cols="57" id="mgsdaterangepicker_custom_scripts" name="mgsdaterangepicker_custom_scripts"><?php echo esc_html( get_option( 'mgsdaterangepicker_custom_scripts' ) ); ?></textarea>
							<p><?php esc_html_e( 'Above script will be inserted just before <code>&lt;/body&gt;</code> tag.', 'mgsdaterangepicker'); ?></p>
							
							<p class="submit">
								<input class="button button-primary" type="submit" name="Submit" value="<?php esc_attr_e( 'Save Settings', 'mgsdaterangepicker'); ?>" />
							</p>
							
						</form>
					</div>
					
					<h2><?php esc_html_e( 'Short Explanation how to write js code in the above Textarea Box for required Date Picker Field', 'mgsdaterangepicker'); ?> - <strong><a href="<?php echo esc_url( "//docs.mgscoder.com/daterange-picker-documentation/" ); ?>" class="button" target="_blank"><?php esc_html_e('View Documentation', 'mgsdaterangepicker'); ?></a></strong></h2>
					<hr />
					<img src="<?php echo esc_url( plugins_url( 'images/js-settings-in-daterange-picker-custom-scripts-1.png' , dirname(__FILE__ )) ); ?>" alt="<?php esc_attr_e( 'js-settings-in-daterange-picker-custom-scripts', 'mgsdaterangepicker'); ?>" width="97%" />
					
				</div>
			</div>

			<?php require_once(MGS_DATERANGEPICKER_PLUGIN_DIR . '/inc/mgsdaterangepicker-sidebar.php'); ?>
		</div>
	</div>
</div>
