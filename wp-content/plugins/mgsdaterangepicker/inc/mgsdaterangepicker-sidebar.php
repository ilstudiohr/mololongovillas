<?php
/*
Plugin Name: DateRange Picker
Plugin URI: https://mgsdemo.mgscoder.com/wp-plugins/daterange-picker-plugin/
Description: Multipurpose Date Range Picker - WordPress Plugin
Author: MGScoder
Author URI: https://codecanyon.net/user/mgscoder?ref=mgscoder
Text Domain: mgsdaterangepicker
*/
?>
<div id="postbox-container-1" class="postbox-container">
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( '< Date Range >: Sample Of JS for form field name="hotelresvdaterange"', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'You can copy &amp; paste of the following code into the left side just will need to replace [name="hotelresvdaterange"] with your Form Field Name. More Documentation and Explanation You will get in documentation folder in purchased', 'mgsdaterangepicker'); ?></p>
<textarea style="width:98%;" rows="6">
var todayDate = moment();
var next3days = moment().add(3, 'days');

$('input[name="hotelresvdaterange"]').daterangepicker({
locale: {
	format: 'MM-DD-YYYY',
	daysOfWeek: [
		"SUN",
		"MON",
		"TUE",
		"WED",
		"THU",
		"FRI",
		"SAT"
	],
	monthNames: [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	],
	firstDay: 0
},
showDropdowns: true,
startDate: todayDate,
endDate: next3days
});</textarea>
		</div>
	</div>
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( '< Single Date >: Sample Of JS for form field name="eventdate"', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'You can copy &amp; paste of the following code into the left side just will need to replace [name="eventdate"] with your Form Field Name. More Documentation and Explanation You will get in documentation folder in purchased', 'mgsdaterangepicker'); ?></p>
<textarea style="width:98%;" rows="6">
var todayDate = moment();

$('input[name="eventdate"]').daterangepicker({
locale: {
	format: 'MM-DD-YYYY',
	daysOfWeek: [
		"SUN",
		"MON",
		"TUE",
		"WED",
		"THU",
		"FRI",
		"SAT"
	],
	monthNames: [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	],
	firstDay: 0
},
showDropdowns: true,
singleDatePicker: true,
startDate: todayDate
});</textarea>
		</div>
	</div>
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( '< Single Date &amp; Time >: Sample Of JS for form field name="tablebookdatetime"', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'You can copy &amp; paste of the following code into the left side just will need to replace [name="tablebookdatetime"] with your Form Field Name. More Documentation and Explanation You will get in documentation folder in purchased', 'mgsdaterangepicker'); ?></p>
<textarea style="width:98%;" rows="6">
var todayDate = moment();

$('input[name="tablebookdatetime"]').daterangepicker({
locale: {
	format: 'MM-DD-YYYY h:mm A',
	daysOfWeek: [
		"SUN",
		"MON",
		"TUE",
		"WED",
		"THU",
		"FRI",
		"SAT"
	],
	monthNames: [
		"January",
		"February",
		"March",
		"April",
		"May",
		"June",
		"July",
		"August",
		"September",
		"October",
		"November",
		"December"
	],
	firstDay: 0
},
showDropdowns: true,
singleDatePicker: true,
timePicker: true,
timePickerIncrement: 5,
startDate: todayDate
});</textarea>
		</div>
	</div>
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( 'Need Help?', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'Read plugin documentation for clarification and how to use it.', 'mgsdaterangepicker'); ?></p>
			<p><strong><a href="<?php echo esc_url( "//docs.mgscoder.com/daterange-picker-documentation/" ); ?>" class="button" target="_blank"><?php esc_html_e('View Documentation', 'mgsdaterangepicker'); ?></a></strong></p>
		</div>
		<div class="inside">
			<p><?php esc_html_e( 'If Need any assist then plaese Contact through Codecanyon Item page Support tab. Support Team will be very happy to assist you.', 'mgsdaterangepicker'); ?></p>
			<p><strong><a href="<?php echo esc_url( "//codecanyon.net/item/daterange-picker-multipurpose-date-range-picker-wordpress-plugin/21468588?ref=mgscoder" ); ?>" class="button" target="_blank"><?php esc_html_e('Support', 'mgsdaterangepicker'); ?></a></strong></p>
		</div>
	</div>
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( 'Rate 5 Stars', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'Like this Plugin Rate it 5 stars with Nice Comments. Your Great Rating will Appreciate to create More Useful Plugins.', 'mgsdaterangepicker'); ?></p>
			<p><a href="<?php echo esc_url( "//codecanyon.net/downloads" ); ?>" class="button" target="_blank"><?php esc_html_e( 'Rate 5 Stars', 'mgsdaterangepicker'); ?></a></p>
		</div>
	</div>
	
	<div class="postbox">
		<h3 class="hndle"><?php esc_html_e( 'Follow Us', 'mgsdaterangepicker'); ?></h3>
		<div class="inside">
			<p><?php esc_html_e( 'Like to get our Latest Product Notification Follow Us.', 'mgsdaterangepicker'); ?></p>
			<p><a href="<?php echo esc_url( "//codecanyon.net/user/mgscoder/follow" ); ?>" class="button" target="_blank"><?php esc_html_e( 'Follow Us', 'mgsdaterangepicker'); ?></a></p>
		</div>
	</div>
	
</div>
