<?php
/**
 * Temeljna konfiguracija WordPressa.
 *
 * wp-config.php instalacijska skripta koristi ovaj zapis tijekom instalacije.
 * Ne morate koristiti web stranicu, samo kopirajte i preimenujte ovaj zapis
 * u "wp-config.php" datoteku i popunite tražene vrijednosti.
 *
 * Ovaj zapis sadrži sljedeće konfiguracije:
 *
 * * MySQL postavke
 * * Tajne ključeve
 * * Prefiks tablica baze podataka
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL postavke - Informacije možete dobiti od vašeg web hosta ** //
/** Ime baze podataka za WordPress */
define('DB_NAME', 'mololong_stage-vil');

/** MySQL korisničko ime baze podataka */
define('DB_USER', 'mololong_stage-v');

/** MySQL lozinka baze podataka */
define('DB_PASSWORD', 'BLd[VUcX1~Ya');

/** MySQL naziv hosta */
define('DB_HOST', 'localhost');

/** Kodna tablica koja će se koristiti u kreiranju tablica baze podataka. */
define('DB_CHARSET', 'utf8');

/** Tip sortiranja (collate) baze podataka. Ne mijenjate ako ne znate što radite. */
define('DB_COLLATE', '');

/**#@+
 * Jedinstveni Autentifikacijski ključevi (Authentication Unique Keys and Salts).
 *
 * Promijenite ovo u vaše jedinstvene fraze!
 * Ključeve možete generirati pomoću {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org servis tajnih-ključeva}
 * Ključeve možete promijeniti bilo kada s tim da će se svi korisnici morati ponovo prijaviti jer kolačići (cookies) neće više važiti nakon izmjene ključeva.
 *
 * @od inačice 2.6.0
 */
define('AUTH_KEY',         '_i{JO{mX(([m<&sM|^5O8p%(~k50%B+PnAZ&[)W{2N4q9|<VyeaQ6=&VT|X|Q] z');
define('SECURE_AUTH_KEY',  '>H)i!MA?A-$f!r|;Q%g.]B^se7w|~|?~u<S{-qh*n| K4!Ds47d3_7-5f7_NZMln');
define('LOGGED_IN_KEY',    'zqw4?7|%dX!)6M5|nV9Y8b&?lWwalumyyKkfvbKnYSxhZ|Ih>|H<9+zJb#kmBSZi');
define('NONCE_KEY',        'l@DD+M@qim3~f9XZT[*j3H.oO`_Lg0&;;X0&7}QvRI :)Q~:|]g+>w7+V7X%h~fw');
define('AUTH_SALT',        'yl~C&NG@jm.PcG/Rf4(/S7KcdmPyodt_@~PaD/P%|>F<+B8|GD+[+J(/b-7|L_[q');
define('SECURE_AUTH_SALT', '^7~eKOa%Mc|7K3FsY6|+z)wihU*5Tk-7C]]elS:!%UHx<WAyVgJERM)+mtt&U`iw');
define('LOGGED_IN_SALT',   'M;]26s7|1^)#Rq8 l*64%.Hy2^>w6jM{_R>(]tHBF4r,] kEHU/W]CG^YF4_fs`0');
define('NONCE_SALT',       'WuUj2;Gki3yHCaw+]*YK`+%Cs}Uie+q6K2e|*&fq1KVYJEtj9eX^BH_iY>|o}-QB');

/**#@-*/

/**
 * Prefix WordPress tablica baze podataka.
 *
 * Možete imati više instalacija unutar jedne baze podataka ukoliko svakoj dodjelite
 * jedinstveni prefiks. Koristite samo brojeve, slova, i donju crticu!
 */
$table_prefix  = 'wp_';

define('WP_POST_REVISIONS', 2);

/**
 * Za programere: WordPress debugging mode.
 *
 * Promijenit ovo u true kako bi omogućili prikazivanje poruka tijekom razvoja.
 * Izrazito preporučujemo da programeri dodataka (plugin) i tema
 * koriste WP_DEBUG u njihovom razvojnom okružju.
 *
 * Za informacije o drugim konstantama koje se mogu koristiti za debugging,
 * posjetite Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/** Memory Limit */
define('WP_MEMORY_LIMIT', '256M');
define( 'WP_MAX_MEMORY_LIMIT', '256M' );

/* To je sve, ne morate više ništa mijenjati! Sretno bloganje. */

/** Apsolutna putanja do WordPress mape. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Postavke za WordPress varijable i već uključene zapise. */
require_once(ABSPATH . 'wp-settings.php');
